﻿using Microsoft.EntityFrameworkCore;
using RushHour.Entities;

namespace RushHour.Data
{
    public class RushHourDbContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<AppointmentActivity> AppointmentActivities { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Appointment> Appointments { get; set; }

        public RushHourDbContext(DbContextOptions<RushHourDbContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<UserRole>()
                .HasOne(x => x.User)
                .WithMany(x => x.UserRoles)
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<UserRole>()
                .HasOne(x => x.Role)
                .WithMany(x => x.UserRoles)
                .HasForeignKey(x => x.RoleId);

            modelBuilder.Entity<AppointmentActivity>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<AppointmentActivity>()
                .HasOne(x => x.Appointment)
                .WithMany(x => x.AppointmentActivities)
                .HasForeignKey(x => x.AppointmentId);

            modelBuilder.Entity<AppointmentActivity>()
                .HasOne(x => x.Activity)
                .WithMany(x => x.AppointmentActivities)
                .HasForeignKey(x => x.ActivityId);


            base.OnModelCreating(modelBuilder);
        }
    }
}

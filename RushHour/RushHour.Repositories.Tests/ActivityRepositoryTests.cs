﻿using RushHour.DomainModels;
using RushHour.Repositories.Tests.Fixtures;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests
{
    public class ActivityRepositoryTests : IClassFixture<ActivityRepositoryFixture>
    {
        private readonly ActivityRepositoryFixture fixture;
        public ActivityRepositoryTests(ActivityRepositoryFixture fixture)
            => this.fixture = fixture;

        [Theory]
        [InlineData("running")]
        [InlineData("lifting")]
        [InlineData("tennis")]
        public async Task WhenGetByNameIfExistsAsyncWithValidId_ShouldReturnValidActivityModel(string name)
        {
            //Act
            var activityResult = await this.fixture.ActivityRepository.GetByNameIfExistsAsync(name);

            //Assert
            Assert.NotNull(activityResult);
            Assert.IsAssignableFrom<ActivityModel>(activityResult);
            Assert.Equal(name, activityResult.Name);
        }

        [Theory]
        [InlineData("invalid")]
        [InlineData("")]
        [InlineData("spane")]
        public async Task WhenGetByNameIfExistsAsyncWithInvalidId_ShouldReturnNull(string name)
        {
            //Act
            var activityResult = await this.fixture.ActivityRepository.GetByNameIfExistsAsync(name);

            //Assert
            Assert.Null(activityResult);
        }

        [Fact]
        public async Task WhenAllNamesExistWithInvalidNames_ShouldReturnFalse()
        {
            //Arrange
            var names = new List<string>
            {
                "invalid", "running", "tennis"
            };

            //Act
            var result = await fixture.ActivityRepository.AllNamesExistAsync(names);

            //
            Assert.False(result);
        }

        [Fact]
        public async Task WhenAllNamesExistWithValidNames_ShouldReturnTrue()
        {
            //Arrange
            var names = new List<string>
            {
                "running", "lifting", "tennis"
            };

            //Act
            var result = await fixture.ActivityRepository.AllNamesExistAsync(names);

            //
            Assert.True(result);
        }
    }
}

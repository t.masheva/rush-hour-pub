﻿using RushHour.DomainModels;
using RushHour.Repositories.Tests.Fixtures;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests
{
    public class UserRepositoryTests : IClassFixture<UserRepositoryFixture>
    {
        private readonly UserRepositoryFixture fixture;
        public UserRepositoryTests(UserRepositoryFixture fixture)
            => this.fixture = fixture;

        [Theory]
        [InlineData("email")]
        [InlineData("emailov")]
        public async Task WhenGetUserByEmailIfExistsAsyncWithValidId_ShouldReturnValidActivityModel(string email)
        {
            //Act
            var userResult = await this.fixture.UserRepository.GetUserByEmailIfExistsAsync(email);

            //Assert
            Assert.NotNull(userResult);
            Assert.IsAssignableFrom<UserModel>(userResult);
            Assert.Equal(email, userResult.Email);
        }

        [Theory]
        [InlineData("invalid")]
        [InlineData("")]
        public async Task WhenGetUserByEmailIfExistsAsyncWithInvalidId_ShouldReturnNull(string email)
        {
            //Act
            var userResult = await this.fixture.UserRepository.GetUserByEmailIfExistsAsync(email);

            //Assert
            Assert.Null(userResult);
        }

        [Theory]
        [InlineData("email")]
        [InlineData("emailov")]
        public async Task WhenGetUserPasswordByEmailAsyncWithValidId_ShouldReturnValidPassword(string email)
        {
            //Act
            var result = await this.fixture.UserRepository.GetUserPasswordByEmailAsync(email);

            //Assert
            Assert.Equal("parola", result);
        }

        [Theory]
        [InlineData("invalid")]
        [InlineData("")]
        public async Task WhenGetUserPasswordByEmailAsyncWithInvalidId_ShouldThrowArgumentException(string email)
        {
            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await this.fixture.UserRepository.GetUserPasswordByEmailAsync(email));
        }

        [Theory]
        [InlineData("invalid")]
        [InlineData("")]
        public async Task WhenGetRolesOfUserAsyncWithInvalidEmail_ShouldThrowArgumentException(string email)
        {
            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await this.fixture.UserRepository.GetRolesOfUserAsync(email));
        }

        [Theory]
        [InlineData("email")]
        [InlineData("emailov")]
        public async Task WhenGetRolesOfUserAsyncWithValidEmail_ShouldSuccessfullyReturnIEnumerable(string email)
        {
            //Act
            var rolesOfUser = await fixture.UserRepository.GetRolesOfUserAsync(email);

            //Assert
            Assert.NotEmpty(rolesOfUser);
            Assert.IsAssignableFrom<IEnumerable<RoleModel>>(rolesOfUser);
        }
    }
}

﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data;
using RushHour.Profiles;
using RushHour.Repositories.Interfaces;
using RushHours.Profiles;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests.Fixtures
{
    public class UserRepositoryFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            var options = new DbContextOptionsBuilder<RushHourDbContext>().UseSqlServer(RepositoryTestContext.TestConnection);
            this.RushHourDbContext = new RushHourDbContext(options.Options);


            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new UserProfile());
                cfg.AddProfile(new RoleProfile());
                cfg.ForAllMaps((map, exp) => exp.MaxDepth(1));
            });

            this.Mapper = config.CreateMapper();

            this.UserRepository = new UserRepository(RushHourDbContext, Mapper);
        }

        public Task DisposeAsync()
        {
            UserRepository = null;
            return Task.CompletedTask;
        }

        public IUserRepository UserRepository { get; private set; }

        public IMapper Mapper { get; private set; }

        public RushHourDbContext RushHourDbContext { get; private set; }
    }
}

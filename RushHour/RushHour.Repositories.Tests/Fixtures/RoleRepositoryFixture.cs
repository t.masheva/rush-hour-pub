﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data;
using RushHour.Profiles;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests.Fixtures
{
    public class RoleRepositoryFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            var options = new DbContextOptionsBuilder<RushHourDbContext>().UseSqlServer(RepositoryTestContext.TestConnection);
            this.RushHourDbContext = new RushHourDbContext(options.Options);


            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new RoleProfile());
                cfg.ForAllMaps((map, exp) => exp.MaxDepth(1));
                });

            this.Mapper = config.CreateMapper();

            this.RoleRepository = new RoleRepository(RushHourDbContext, Mapper);
        }

        public Task DisposeAsync()
        {
            RoleRepository = null;
            return Task.CompletedTask;
        }

        public IRoleRepository RoleRepository { get; private set; }

        public IMapper Mapper { get; private set; }

        public RushHourDbContext RushHourDbContext { get; private set; }
    }
}

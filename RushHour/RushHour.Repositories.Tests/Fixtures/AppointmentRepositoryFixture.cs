﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data;
using RushHour.Profiles;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests.Fixtures
{
    public class AppointmentRepositoryFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            var options = new DbContextOptionsBuilder<RushHourDbContext>().UseSqlServer(RepositoryTestContext.TestConnection);
            this.RushHourDbContext = new RushHourDbContext(options.Options);


            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new ActivityProfile());
                cfg.AddProfile(new AppointmentActivityProfile());
                cfg.AddProfile(new AppointmentProfile());
                cfg.ForAllMaps((map, exp) => exp.MaxDepth(1));
                });

            this.Mapper = config.CreateMapper();

            this.AppointmentRepository = new AppointmentRepository(RushHourDbContext, Mapper);
        }

        public Task DisposeAsync()
        {
            AppointmentRepository = null;
            return Task.CompletedTask;
        }

        public IAppointmentRepository AppointmentRepository { get; private set; }

        public IMapper Mapper { get; private set; }

        public RushHourDbContext RushHourDbContext { get; private set; }
    
    }
}

﻿
using RushHour.DomainModels;
using RushHour.Models.Enums;
using RushHour.Repositories.Tests.Fixtures;
using System;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests
{
    public class RoleRepositoryTests : IClassFixture<RoleRepositoryFixture>
    {
        private readonly RoleRepositoryFixture fixture;
        public RoleRepositoryTests(RoleRepositoryFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenGetIdAsyncWithInvalidModel_ShouldthrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.RoleRepository.GetIdAsync(null));
        }

        [Fact]
        public async Task WhenGetIdAsyncWithValidModel_ShouldSuccessfullyReturnRole()
        {
            //Arrange
            var role = new RoleModel
            {
                Name = RoleEnum.admin.ToString()
            };

            //Act
            var id = await fixture.RoleRepository.GetIdAsync(role);

            //Assert
            Assert.Equal(2, id);
        }

        [Theory]
        [InlineData("invalid")]
        [InlineData("")]
        public async Task WhenRoleByNameExistsAsyncWithInvalidName_ShouldReturnFalse(string name)
        {
            //Act
            var result = await fixture.RoleRepository.RoleByNameExistsAsync(name);

            //Assert
            Assert.False(result);
        }

        [Theory]
        [InlineData(RoleEnum.user)]
        [InlineData(RoleEnum.admin)]
        public async Task WhenRoleByNameExistsAsyncWithValidName_ShouldReturnTrue(RoleEnum name)
        {
            //Act
            var result = await fixture.RoleRepository.RoleByNameExistsAsync(name.ToString());

            //Assert
            Assert.True(result);
        }
    }
}

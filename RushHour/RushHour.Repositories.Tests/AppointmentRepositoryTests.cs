using RushHour.DomainModels;
using RushHour.Repositories.Tests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Repositories.Tests
{
    public class AppointmentRepositoryTests : IClassFixture<AppointmentRepositoryFixture>
    {
        private readonly AppointmentRepositoryFixture fixture;
        public AppointmentRepositoryTests(AppointmentRepositoryFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenGetAllAsync_ShouldReturnAllAppointments()
        {
            //Act
            var appointmentsResult = await this.fixture.AppointmentRepository.GetAllAsync();

            //Assert
            Assert.NotNull(appointmentsResult);
            Assert.IsAssignableFrom<IEnumerable<AppointmentModel>>(appointmentsResult);
            Assert.Equal(4, appointmentsResult.Count());
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(-10)]
        [InlineData(int.MaxValue)]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidId_ShouldReturnNull(int id)
        {
            //Act
            var appointmentResult = await this.fixture.AppointmentRepository.GetByIdIfExistsAsync(id);

            //Assert
            Assert.Null(appointmentResult);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(6)]
        [InlineData(8)]
        [InlineData(9)]
        public async Task WhenGetByIdIfExistsAsyncWithValidId_ShouldReturnValidAppointmentModel(int id)
        {
            //Act
            var appointmentResult = await this.fixture.AppointmentRepository.GetByIdIfExistsAsync(id);

            //Assert
            Assert.NotNull(appointmentResult);
            Assert.IsAssignableFrom<AppointmentModel>(appointmentResult);
        }

        [Fact]
        public async Task WhenAddAsyncWithValidModel_ShouldReturnAppointment()
        {
            //Arrange
            var appointmentModel = new AppointmentModel
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now,
                UserId = 1
            };

            //Act
            var appointmentResult = await this.fixture.AppointmentRepository.AddAsync(appointmentModel);

            //Assert
            Assert.NotNull(appointmentResult);
            Assert.NotEqual((int)default, appointmentResult.Id);
            Assert.IsAssignableFrom<AppointmentModel>(appointmentResult);
            Assert.Equal(appointmentModel.EndDate, appointmentResult.EndDate);
            Assert.Equal(appointmentModel.StartDate, appointmentResult.StartDate);
            Assert.Equal(appointmentModel.UserId, appointmentResult.UserId);
        }

        [Fact]
        public async Task WhenAddAsyncWithInvalidModel_ShouldthrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.AppointmentRepository.AddAsync(null));
        }

        [Fact]
        public async Task WhenUpdateAsyncWithValidModel_ShouldSuccessfullyUpdateAppointment()
        {
            //Arrange
            var appointment = fixture.RushHourDbContext.Appointments.AsEnumerable().Last();
            var appointmentModel = new AppointmentModel
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now,
                UserId = appointment.UserId,
                Id = appointment.Id
            };

            //Act
            await this.fixture.AppointmentRepository.UpdateAsync(appointmentModel);
            var updatedAppointment = await fixture.AppointmentRepository.GetByIdIfExistsAsync(appointment.Id);

            //Assert
            Assert.NotNull(updatedAppointment);
            Assert.Equal(appointmentModel.EndDate, updatedAppointment.EndDate);
            Assert.Equal(appointmentModel.StartDate, updatedAppointment.StartDate);
            Assert.Equal(appointmentModel.UserId, updatedAppointment.UserId);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithInvalidModel_ShouldthrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.AppointmentRepository.UpdateAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithValidId_ShouldSuccessfullyDeleteAppointment()
        {
            //Arrange
            var appointment = fixture.RushHourDbContext.Appointments.AsEnumerable().Last();

            //Act
            await this.fixture.AppointmentRepository.DeleteAsync(appointment.Id);
            var deletedAppointment = await fixture.AppointmentRepository.GetByIdIfExistsAsync(appointment.Id);

            //Assert
            Assert.Null(deletedAppointment);
        }


        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(-10)]
        [InlineData(int.MaxValue)]
        public async Task WhenDeleteAsyncWithInvalidId_ShouldthrowArgumentException(int id)
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentRepository.DeleteAsync(id));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-10)]
        [InlineData(int.MaxValue)]
        public async Task WhenGetAppointmentsOfUserAsyncWithInvalidId_ShouldReturnEmptyIEnumerable(int id)
        {
            //Act
            var appointmentsOfUser = await fixture.AppointmentRepository.GetAppointmentsOfUserAsync(id);

            //Assert
            Assert.Empty(appointmentsOfUser);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task WhenGetAppointmentsOfUserAsyncWithValidId_ShouldSuccessfullyReturnIEnumerable(int id)
        {
            //Act
            var appointmentsOfUser = await fixture.AppointmentRepository.GetAppointmentsOfUserAsync(id);

            //Assert
            Assert.NotEmpty(appointmentsOfUser);
            Assert.IsAssignableFrom<IEnumerable<AppointmentModel>>(appointmentsOfUser);
        }
    }
}

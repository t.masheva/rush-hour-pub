using Moq;
using RushHour.Domain.Tests.Fixtures;
using RushHour.DomainModels;
using RushHour.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class RoleServiceTests : IClassFixture<RoleServiceFixture>
    {
        private readonly RoleServiceFixture fixture;
        public RoleServiceTests(RoleServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenAddAsync_ThenReturnCreatedRole()
        {
            //Arange
            var roleModel = new RoleModel
            {
                Id = 1,
                Name = RoleEnum.user.ToString()
            };

            fixture.RoleRepositoryMock.Setup(s => s.AddAsync(It.IsAny<RoleModel>()))
                .ReturnsAsync(new RoleModel
                {
                    Id = roleModel.Id,
                    Name = roleModel.Name
                });

            //Act
            var result = await fixture.RoleService.AddAsync(roleModel);

            //Assert
            Assert.Equal(roleModel.Id, result.Id);
            Assert.Equal(roleModel.Name, result.Name);
        }

        [Fact]
        public async Task WhenAddAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.RoleService.AddAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithNonexistentId_ThrowsArgumentException()
        {
            //Arrange
            var role = (RoleModel)default;
            fixture.RoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(role);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.RoleService.DeleteAsync(1));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithCorrectId_ThenCallDeleteAsyncInRepository()
        {
            //Arrange
            var role = new RoleModel();
            fixture.RoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(role);

            //Act
            await fixture.RoleService.DeleteAsync(1);

            //Assert
            fixture.RoleRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithValidId_ThenReturnRole()
        {
            //Arrange
            var roleId = 1;
            var role = new RoleModel
            {
                Id = roleId,
                Name = RoleEnum.user.ToString()
            };

            fixture.RoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(role);

            //Act
            var roleResult = await fixture.RoleService.GetByIdIfExistsAsync(roleId);

            //Assert
            Assert.Equal(roleResult, role);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidId_ThenReturnDefault()
        {
            //Arrange
            var roleId = -101;
            var role = (RoleModel)default;

            fixture.RoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(role);

            //Act
            var roleResult = await fixture.RoleService.GetByIdIfExistsAsync(roleId);

            //Assert
            Assert.Equal(roleResult, role);
        }

        [Fact]
        public async Task WhenUpdateAsync_ThenReturnEditRole()
        {
            //Arrange
            var roleModel = new RoleModel
            {
                Id = 1,
                Name = RoleEnum.user.ToString()
            };
            fixture.RoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(roleModel);

            //Act
            await fixture.RoleService.UpdateAsync(roleModel);

            //Assert
            fixture.RoleRepositoryMock.Verify(x => x.UpdateAsync(roleModel), Times.Once);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.RoleService.UpdateAsync(null));
        }


        [Fact]
        public async Task WhenRoleByNameExistsAsyncWithValidName_ThenReturnTrue()
        {
            //Arrange
            var roleName = RoleEnum.user.ToString();

            fixture.RoleRepositoryMock.Setup(s => s.RoleByNameExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(true);

            //Act
            var result = await fixture.RoleService.RoleByNameExistsAsync(roleName);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public async Task WhenRoleByNameExistsAsyncWithInvalidName_ThenReturnFalse()
        {
            //Arrange
            var roleName = "invalid";

            fixture.RoleRepositoryMock.Setup(s => s.RoleByNameExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(false);

            //Act
            var result = await fixture.RoleService.RoleByNameExistsAsync(roleName);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public async Task WhenGetIdAsyncWithNull_ThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.RoleService.GetIdAsync(null));
        }

        [Fact]
        public async Task WhenGetIdAsyncWithRole_ThenSuccessfullyReturnId()
        {
            //Arrange
            var roleId = 1;
            var roleModel = new RoleModel
            {
                Id = 1,
                Name = RoleEnum.user.ToString()
            };
            fixture.RoleRepositoryMock.Setup(s => s.GetIdAsync(It.IsAny<RoleModel>()))
                .ReturnsAsync(roleId);

            //Act
            var result = await fixture.RoleService.GetIdAsync(roleModel);

            //Assert
            Assert.Equal(result, roleId);
        }


        [Fact]
        public async Task WhenUpdateAsyncWithInvalidId_ThenThrowArgumentException()
        {
            //Arrange
            var roleModel = new RoleModel
            {
                Id = 100,
                Name = RoleEnum.user.ToString()
            };

            fixture.RoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((RoleModel)default);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.RoleService.UpdateAsync(roleModel));
        }
    }
}

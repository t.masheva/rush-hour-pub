using Moq;
using RushHour.Domain.Tests.Fixtures;
using RushHour.DomainModels;
using System;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class AppointmentActivityServiceTests : IClassFixture<AppointmentActivityServiceFixture>
    {
        private readonly AppointmentActivityServiceFixture fixture;
        public AppointmentActivityServiceTests(AppointmentActivityServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenAddAsync_ThenReturnCreatedAppointmentActivity()
        {
            //Arange
            var appointmentActivityModel = new AppointmentActivityModel
            {
                Id = 1,
                ActivityId = 1,
                AppointmentId = 1
            };

            fixture.AppointmentActivityRepositoryMock.Setup(s => s.AddAsync(It.IsAny<AppointmentActivityModel>()))
                .ReturnsAsync(new AppointmentActivityModel
                {
                    Id = appointmentActivityModel.Id,
                    ActivityId = appointmentActivityModel.ActivityId,
                    AppointmentId = appointmentActivityModel.AppointmentId
                });

            //Act
            var result = await fixture.AppointmentActivityService.AddAsync(appointmentActivityModel);

            //Assert
            Assert.Equal(appointmentActivityModel.Id, result.Id);
            Assert.Equal(appointmentActivityModel.ActivityId, result.ActivityId);
            Assert.Equal(appointmentActivityModel.AppointmentId, result.AppointmentId);
        }

        [Fact]
        public async Task WhenAddAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.AppointmentActivityService.AddAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithNonexistentId_ThrowsArgumentException()
        {
            //Arrange
            var appointmentActivity = (AppointmentActivityModel)default;
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivity);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentActivityService.DeleteAsync(1));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithCorrectId_ThenCallDeleteAsyncInRepository()
        {
            //Arrange
            var appointmentActivity = new AppointmentActivityModel();
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivity);

            //Act
            await fixture.AppointmentActivityService.DeleteAsync(1);

            //Assert
            fixture.AppointmentActivityRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithValidId_ThenReturnAppointmentActivity()
        {
            //Arrange
            var appointmentActivityId = 1;
            var appointmentActivity = new AppointmentActivityModel
            {
                Id = appointmentActivityId,
                ActivityId = 1,
                AppointmentId = 1
            };

            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivity);

            //Act
            var appointmentActivityResult = await fixture.AppointmentActivityService.GetByIdIfExistsAsync(appointmentActivityId);

            //Assert
            Assert.Equal(appointmentActivityResult, appointmentActivity);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidId_ThenReturnDefault()
        {
            //Arrange
            var appointmentActivityId = -101;
            var appointmentActivity = (AppointmentActivityModel)default;

            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivity);

            //Act
            var appointmentActivityResult = await fixture.AppointmentActivityService.GetByIdIfExistsAsync(appointmentActivityId);

            //Assert
            Assert.Equal(appointmentActivityResult, appointmentActivity);
        }

        [Fact]
        public async Task WhenUpdateAsync_ThenReturnEditAppointmentActivity()
        {
            //Arrange
            var appointmentActivityModel = new AppointmentActivityModel
            {
                Id = 1,
                ActivityId = 1,
                AppointmentId = 1
            };
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivityModel);

            //Act
            await fixture.AppointmentActivityService.UpdateAsync(appointmentActivityModel);

            //Assert
            fixture.AppointmentActivityRepositoryMock.Verify(x => x.UpdateAsync(appointmentActivityModel), Times.Once);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.AppointmentActivityService.UpdateAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAppointmentsOfActivityAsyncWithInvalidId_ThenThrowArgumenException()
        {
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((AppointmentActivityModel)default);

            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentActivityService.DeleteAppointmentsOfActivityAsync(-100));
        }

        [Fact]
        public async Task WhenDeleteActivitiesOfAppointmentAsyncWithInvalidId_ThenThrowArgumenException()
        {
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((AppointmentActivityModel)default);

            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentActivityService.DeleteActivitiesOfAppointmentAsync(-100));
        }

        [Fact]
        public async Task WhenDeleteActivitiesOfAppointmentAsyncWithValidId_ThenSuccessfullyDelete()
        {
            //Arrange
            var appointmentActivityModel = new AppointmentActivityModel
            {
                Id = 1,
                ActivityId = 1,
                AppointmentId = 1
            };
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivityModel);

            //Act
            await fixture.AppointmentActivityService.DeleteActivitiesOfAppointmentAsync(appointmentActivityModel.AppointmentId);

            //Assert
            fixture.AppointmentActivityRepositoryMock.Verify(x => x.DeleteActivitiesOfAppointmentAsync(appointmentActivityModel.AppointmentId), Times.Once);
        }
        [Fact]

        public async Task WhenDeleteAppointmentsOfActivityAsyncWithValidId_ThenSuccessfullyDelete()
        {
            //Arrange
            var appointmentActivityModel = new AppointmentActivityModel
            {
                Id = 1,
                ActivityId = 1,
                AppointmentId = 1
            };
            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentActivityModel);

            //Act
            await fixture.AppointmentActivityService.DeleteAppointmentsOfActivityAsync(appointmentActivityModel.ActivityId);

            //Assert
            fixture.AppointmentActivityRepositoryMock.Verify(x => x.DeleteAppointmentsOfActivityAsync(appointmentActivityModel.ActivityId), Times.Once);
        }


        [Fact]
        public async Task WhenUpdateAsyncWithInvalidId_ThenThrowArgumentException()
        {
            //Arrange
            var appointmentActivityModel = new AppointmentActivityModel
            {
                Id = 1,
                ActivityId = 1,
                AppointmentId = 1
            };

            fixture.AppointmentActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((AppointmentActivityModel)default);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentActivityService.UpdateAsync(appointmentActivityModel));
        }
    }
}

using Moq;
using RushHour.Domain.Tests.Fixtures;
using RushHour.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class UserRoleServiceTests : IClassFixture<UserRoleServiceFixture>
    {
        private readonly UserRoleServiceFixture fixture;
        public UserRoleServiceTests(UserRoleServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenAddAsync_ThenReturnCreatedUserRole()
        {
            //Arange
            var userRoleModel = new UserRoleModel
            {
                UserId = 1,
                RoleId = 1
            };

            fixture.UserRoleRepositoryMock.Setup(s => s.AddAsync(It.IsAny<UserRoleModel>()))
                .ReturnsAsync(new UserRoleModel
                {
                    UserId = userRoleModel.UserId,
                    RoleId = userRoleModel.RoleId,
                    Id = 1
                });

            //Act
            var result = await fixture.UserRoleService.AddAsync(userRoleModel);

            //Assert
            Assert.Equal(userRoleModel.UserId, result.UserId);
            Assert.Equal(userRoleModel.RoleId, result.RoleId);
        }

        [Fact]
        public async Task WhenAddAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.UserRoleService.AddAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithNonexistentId_ThrowsArgumentException()
        {
            //Arrange
            var userRole = (UserRoleModel)default;
            fixture.UserRoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userRole);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.UserRoleService.DeleteAsync(1));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithCorrectId_ThenCallDeleteAsyncInRepository()
        {
            //Arrange
            var userRole = new UserRoleModel();
            fixture.UserRoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userRole);

            //Act
            await fixture.UserRoleService.DeleteAsync(1);

            //Assert
            fixture.UserRoleRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithValidUserId_ThenReturnUserRole()
        {
            //Arrange
            var userRoleId = 1;
            var userRole = new UserRoleModel
            {
                Id = userRoleId,
                UserId = 1,
                RoleId = 1
            };

            fixture.UserRoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userRole);

            //Act
            var userRoleResult = await fixture.UserRoleService.GetByIdIfExistsAsync(userRoleId);

            //Assert
            Assert.Equal(userRoleResult, userRole);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidUserId_ThenReturnDefault()
        {
            //Arrange
            var userRoleId = -101;
            var userRole = (UserRoleModel)default;

            fixture.UserRoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userRole);

            //Act
            var userRoleResult = await fixture.UserRoleService.GetByIdIfExistsAsync(userRoleId);

            //Assert
            Assert.Equal(userRoleResult, userRole);
        }

        [Fact]
        public async Task WhenUpdateAsync_ThenReturnEditUserRole()
        {
            //Arrange
            var userRoleModel = new UserRoleModel
            {
                UserId = 1,
                RoleId = 1
            };
            fixture.UserRoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userRoleModel);

            //Act
            await fixture.UserRoleService.UpdateAsync(userRoleModel);

            //Assert
            fixture.UserRoleRepositoryMock.Verify(x => x.UpdateAsync(userRoleModel), Times.Once);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.UserRoleService.UpdateAsync(null));
        }


        [Fact]
        public async Task WhenUpdateAsyncWithInvalidId_ThenThrowArgumentException()
        {
            //Arrange
            var userRoleModel = new UserRoleModel
            {
                UserId = 1,
                RoleId = 1,
                Id = 100
            };
            fixture.UserRoleRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((UserRoleModel)default);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.UserRoleService.UpdateAsync(userRoleModel));
        }
    }
}

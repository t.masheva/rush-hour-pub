﻿using Moq;
using RushHour.Domain.Infrastructure;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class SecurityServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.JwtSettingsMock = new Mock<IJwtSettings>();

            this.SecurityService = new SecurityService(JwtSettingsMock.Object);
        }

        public Task DisposeAsync()
        {
            SecurityService = null;
            return Task.CompletedTask;
        }

        public ISecurityService SecurityService { get; private set; }

        public Mock<IJwtSettings> JwtSettingsMock { get; private set; }
    }
}

﻿using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class RoleServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.RoleRepositoryMock = new Mock<IRoleRepository>();

            this.RoleService = new RoleService(RoleRepositoryMock.Object);
        }

        public Task DisposeAsync()
        {
            RoleService = null;
            return Task.CompletedTask;
        }

        public IRoleService RoleService { get; private set; }

        public Mock<IRoleRepository> RoleRepositoryMock { get; private set; }
    }
}

﻿using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class ActivityServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.ActivityRepositoryMock = new Mock<IActivityRepository>();

            this.ActivityService = new ActivityService(ActivityRepositoryMock.Object);
        }

        public Task DisposeAsync()
        {
            ActivityService = null;
            return Task.CompletedTask;
        }

        public IActivityService ActivityService { get; private set; }

        public Mock<IActivityRepository> ActivityRepositoryMock { get; private set; }
    }
}

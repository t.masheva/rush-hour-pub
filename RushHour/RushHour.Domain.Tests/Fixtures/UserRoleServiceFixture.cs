﻿using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class UserRoleServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.UserRoleRepositoryMock = new Mock<IUserRoleRepository>();

            this.UserRoleService = new UserRoleService(UserRoleRepositoryMock.Object);
        }

        public Task DisposeAsync()
        {
            UserRoleService = null;
            return Task.CompletedTask;
        }

        public IUserRoleService UserRoleService { get; private set; }

        public Mock<IUserRoleRepository> UserRoleRepositoryMock { get; private set; }
    }
}

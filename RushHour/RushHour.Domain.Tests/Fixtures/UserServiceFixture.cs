﻿using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class UserServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.UserRepositoryMock = new Mock<IUserRepository>();
            this.RoleServiceMock = new Mock<IRoleService>();
            this.UserRoleServiceMock = new Mock<IUserRoleService>();

            this.UserService = new UserService(UserRepositoryMock.Object, UserRoleServiceMock.Object, RoleServiceMock.Object);
        }

        public Task DisposeAsync()
        {
            UserService = null;
            return Task.CompletedTask;
        }

        public IUserService UserService { get; private set; }

        public Mock<IUserRepository> UserRepositoryMock { get; private set; }

        public Mock<IRoleService> RoleServiceMock { get; private set; }

        public Mock<IUserRoleService> UserRoleServiceMock { get; private set; }
    }
}

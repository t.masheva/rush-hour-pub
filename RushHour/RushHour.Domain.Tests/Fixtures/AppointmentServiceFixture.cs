﻿using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class AppointmentServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.AppointmentRepositoryMock = new Mock<IAppointmentRepository>();

            this.UserServiceMock = new Mock<IUserService>();

            this.AppointmentService = new AppointmentService(AppointmentRepositoryMock.Object, UserServiceMock.Object);
        }

        public Task DisposeAsync()
        {
            AppointmentService = null;
            return Task.CompletedTask;
        }

        public IAppointmentService AppointmentService { get; private set; }

        public Mock<IAppointmentRepository> AppointmentRepositoryMock { get; private set; }

        public Mock<IUserService> UserServiceMock { get; private set; }
    }
}

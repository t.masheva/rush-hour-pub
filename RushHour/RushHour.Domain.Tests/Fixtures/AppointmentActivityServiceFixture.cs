﻿using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Repositories.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests.Fixtures
{
    public class AppointmentActivityServiceFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.AppointmentActivityRepositoryMock = new Mock<IAppointmentActivityRepository>();

            this.AppointmentActivityService = new AppointmentActivityService(AppointmentActivityRepositoryMock.Object);
        }

        public Task DisposeAsync()
        {
            AppointmentActivityService = null;
            return Task.CompletedTask;
        }

        public IAppointmentActivityService AppointmentActivityService { get; private set; }

        public Mock<IAppointmentActivityRepository> AppointmentActivityRepositoryMock { get; private set; }
    }
}

using Moq;
using RushHour.Domain.Tests.Fixtures;
using RushHour.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class ActivityServiceTests : IClassFixture<ActivityServiceFixture>
    {
        private readonly ActivityServiceFixture fixture;
        public ActivityServiceTests(ActivityServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenAddAsync_ThenReturnCreatedActivity()
        {
            //Arange
            var activityModel = new ActivityModel
            {
                Duration = 10.5m,
                Name = "running",
                Price = 20
            };

            fixture.ActivityRepositoryMock.Setup(s => s.AddAsync(It.IsAny<ActivityModel>()))
                .ReturnsAsync(new ActivityModel
                {
                    Duration = activityModel.Duration,
                    Name = activityModel.Name,
                    Price = activityModel.Price,
                    Id = 1
                });

            //Act
            var result = await fixture.ActivityService.AddAsync(activityModel);

            //Assert
            Assert.Equal(activityModel.Duration, result.Duration);
            Assert.Equal(activityModel.Name, result.Name);
            Assert.Equal(activityModel.Price, result.Price);
        }

        [Fact]
        public async Task WhenAddAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.ActivityService.AddAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithNonexistentId_ThrowsArgumentException()
        {
            //Arrange
            var activity = (ActivityModel)default;
            fixture.ActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activity);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.ActivityService.DeleteAsync(1));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithCorrectId_ThenCallDeleteAsyncInRepository()
        {
            //Arrange
            var activity = new ActivityModel();
            fixture.ActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activity);

            //Act
            await fixture.ActivityService.DeleteAsync(1);

            //Assert
            fixture.ActivityRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task WheGetAllAsyncWithCorrectId_ThenCallGetAllAsyncInRepository()
        {
            //Act
            await fixture.ActivityService.GetAllAsync();

            //Assert
            fixture.ActivityRepositoryMock.Verify(x => x.GetAllAsync(), Times.Once);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithValidUserId_ThenReturnActivity()
        {
            //Arrange
            var activityId = 1;
            var activity = new ActivityModel
            {
                Id = activityId,
                Duration = 10.5m,
                Name = "running",
                Price = 20
            };

            fixture.ActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activity);

            //Act
            var activityResult = await fixture.ActivityService.GetByIdIfExistsAsync(activityId);

            //Assert
            Assert.Equal(activityResult, activity);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidUserId_ThenReturnDefault()
        {
            //Arrange
            var activityId = -101;
            var activity = (ActivityModel)default;

            fixture.ActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activity);

            //Act
            var activityResult = await fixture.ActivityService.GetByIdIfExistsAsync(activityId);

            //Assert
            Assert.Equal(activityResult, activity);
        }

        [Fact]
        public async Task WhenUpdateAsync_ThenReturnEditActivity()
        {
            //Arrange
            var activityModel = new ActivityModel
            {
                Duration = 10.5m,
                Name = "running",
                Price = 20
            };
            fixture.ActivityRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activityModel);

            //Act
            await fixture.ActivityService.UpdateAsync(activityModel);

            //Assert
            fixture.ActivityRepositoryMock.Verify(x => x.UpdateAsync(activityModel), Times.Once);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.ActivityService.UpdateAsync(null));
        }


        [Fact]
        public async Task WhenGetByNameIfExistsAsyncWithValidName_ThenReturnActivity()
        {
            //Arrange
            var activityId = 1;
            var activity = new ActivityModel
            {
                Id = activityId,
                Duration = 10.5m,
                Name = "running",
                Price = 20
            };

            fixture.ActivityRepositoryMock.Setup(s => s.GetByNameIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(activity);

            //Act
            var activityResult = await fixture.ActivityService.GetByNameIfExistsAsync(activity.Name);

            //Assert
            Assert.Equal(activityResult, activity);
        }

        [Fact]
        public async Task WhenGetByNameIfExistsAsyncWithInvalidName_ThenReturnDefault()
        {
            //Arrange
            var activityName = "invalid";
            var activity = (ActivityModel)default;

            fixture.ActivityRepositoryMock.Setup(s => s.GetByNameIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(activity);

            //Act
            var activityResult = await fixture.ActivityService.GetByNameIfExistsAsync(activityName);

            //Assert
            Assert.Equal(activityResult, activity);
        }

        [Fact]
        public async Task WhenAllNamesExistAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.ActivityService.AllNamesExistAsync(null));
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task WhenAllNamesExistAsyncWithCollection_ThenReturnIfActivitiesExist(bool allNamesExist)
        {
            //Arrange
            var names = new List<string>
            {
                "running", "lifting", "tennis"
            };
            fixture.ActivityRepositoryMock.Setup(s => s.AllNamesExistAsync(It.IsAny<ICollection<string>>())).ReturnsAsync(allNamesExist);

            //Act
            var result = await this.fixture.ActivityService.AllNamesExistAsync(names);

            //Assert
            Assert.Equal(allNamesExist, result);

        }
    }
}

﻿using Microsoft.IdentityModel.Tokens;
using RushHour.Domain.Tests.Fixtures;
using RushHour.Models.Enums;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class SecurityServiceTests : IClassFixture<SecurityServiceFixture>
    {
        private readonly SecurityServiceFixture fixture;
        public SecurityServiceTests(SecurityServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public void WhenGenerateJwtToken_ShouldSuccessfullyReturnToken()
        {
            //Arrange
            var email = "email";
            var roles = new List<string>
            {
                RoleEnum.user.ToString(), RoleEnum.admin.ToString()
            };

            var key = "1E8C9C56-%*?.-429D-+-/$-FE4207FDFEDD";
            var audience = "https://localhost";
            var issuer = "https://localhost";
            var expiration = 120;
            fixture.JwtSettingsMock.Setup(s => s.Key).Returns(key);
            fixture.JwtSettingsMock.Setup(s => s.Audience).Returns(audience);
            fixture.JwtSettingsMock.Setup(s => s.Issuer).Returns(issuer);
            fixture.JwtSettingsMock.Setup(s => s.ExpirationInMinutes).Returns(expiration);

            var expires = DateTime.UtcNow.Add(TimeSpan.FromMinutes(expiration));

            //Act
            var tokenString = fixture.SecurityService.GenerateJwtToken(email, roles);
            var token = new JwtSecurityTokenHandler().ReadJwtToken(tokenString);
            var tokenRoles = token.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value);

            //Assert
            Assert.Contains(RoleEnum.user.ToString(), tokenRoles);
            Assert.Contains(RoleEnum.admin.ToString(), tokenRoles);
            Assert.Equal(issuer, token.Issuer);
            Assert.Equal(expires.ToLongDateString(), token.ValidTo.ToLongDateString());
            Assert.Equal(email, token.Claims.First(c => c.Type == JwtRegisteredClaimNames.Sub).Value);
            Assert.Equal(audience, token.Claims.First(c => c.Type == JwtRegisteredClaimNames.Aud).Value);
        }

    }
}

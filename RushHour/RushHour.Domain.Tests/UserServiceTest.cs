using Moq;
using RushHour.Domain.Tests.Fixtures;
using RushHour.DomainModels;
using RushHour.Helpers;
using RushHour.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class UserServiceTests : IClassFixture<UserServiceFixture>
    {
        private readonly UserServiceFixture fixture;
        public UserServiceTests(UserServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenAddAsync_ThenReturnCreatedUser()
        {
            //Arange
            var userModel = new UserModel
            {
                Email = "email",
                FirstName = "John",
                LastName = "Smith",
                Password = "password",
                Id = 1
            };

            fixture.UserRepositoryMock.Setup(s => s.AddAsync(It.IsAny<UserModel>()))
                .ReturnsAsync(new UserModel
                {
                    Email = "email",
                    FirstName = "John",
                    LastName = "Smith",
                    Password = "password",
                    Id = 1
                });

            //Act
            var result = await fixture.UserService.AddAsync(userModel);

            //Assert
            Assert.Equal(userModel.Email, result.Email);
            Assert.Equal(userModel.FirstName, result.FirstName);
            Assert.Equal(userModel.Password, result.Password);
            Assert.Equal(userModel.LastName, result.LastName);
            Assert.Equal(userModel.Id, result.Id);
        }

        [Fact]
        public async Task WhenAddAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.UserService.AddAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithNonexistentId_ThrowsArgumentException()
        {
            //Arrange
            var user = (UserModel)default;
            fixture.UserRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(user);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.UserService.DeleteAsync(1));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithCorrectId_ThenCallDeleteAsyncInRepository()
        {
            //Arrange
            var user = new UserModel();
            fixture.UserRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(user);

            //Act
            await fixture.UserService.DeleteAsync(1);

            //Assert
            fixture.UserRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithValidUserId_ThenReturnUser()
        {
            //Arrange
            var userId = 1;
            var user = new UserModel
            {
                Id = userId,
                Email = "email",
                FirstName = "John",
                LastName = "Smith",
                Password = "password"
            };

            fixture.UserRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(user);

            //Act
            var userResult = await fixture.UserService.GetByIdIfExistsAsync(userId);

            //Assert
            Assert.Equal(userResult, user);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidUserId_ThenReturnDefault()
        {
            //Arrange
            var userId = -101;
            var user = (UserModel)default;

            fixture.UserRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(user);

            //Act
            var userResult = await fixture.UserService.GetByIdIfExistsAsync(userId);

            //Assert
            Assert.Equal(userResult, user);
        }

        [Fact]
        public async Task WhenUpdateAsync_ThenReturnEditUser()
        {
            //Arrange
            var userModel = new UserModel
            {
                Email = "email",
                FirstName = "John",
                LastName = "Smith",
                Password = "password",
                Id = 1
            };
            fixture.UserRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userModel);

            //Act
            await fixture.UserService.UpdateAsync(userModel);

            //Assert
            fixture.UserRepositoryMock.Verify(x => x.UpdateAsync(userModel), Times.Once);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.UserService.UpdateAsync(null));
        }


        [Fact]
        public async Task WhenUpdateAsyncWithInvalidId_ThenThrowArgumentException()
        {
            //Arrange
            var userModel = new UserModel
            {
                Email = "email",
                FirstName = "John",
                LastName = "Smith",
                Password = "password",
                Id = 100
            };
            fixture.UserRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((UserModel)default);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.UserService.UpdateAsync(userModel));
        }

        [Fact]
        public async Task WhenGetUserByEmailIfExistsAsyncWithValidUserEmail_ThenReturnUser()
        {
            //Arrange
            var user = new UserModel
            {
                Id = 1,
                Email = "email",
                FirstName = "John",
                LastName = "Smith",
                Password = "password"
            };

            fixture.UserRepositoryMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            //Act
            var userResult = await fixture.UserService.GetUserByEmailIfExistsAsync(user.Email);

            //Assert
            Assert.Equal(userResult, user);
        }

        [Fact]
        public async Task WhenGetUserByEmailIfExistsAsyncWithInvalidUserEmail_ThenReturnDefault()
        {
            //Arrange
            var userEmail = "invalid";
            var user = (UserModel)default;

            fixture.UserRepositoryMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            //Act
            var userResult = await fixture.UserService.GetUserByEmailIfExistsAsync(userEmail);

            //Assert
            Assert.Equal(userResult, user);
        }
        [Fact]
        public async Task WhenGetRolesOfUserAsyncWithValidUserEmail_ThenReturnUser()
        {
            //Arrange
            var roles = new List<RoleModel>();
            var roleNames = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                roles.Add(new RoleModel
                {
                    Id = i + 1,
                    Name = RoleEnum.user.ToString()
                });
                roleNames.Add(RoleEnum.user.ToString());
            }

            fixture.UserRepositoryMock.Setup(s => s.GetRolesOfUserAsync(It.IsAny<string>()))
                .ReturnsAsync(roles);

            //Act
            var userResult = await fixture.UserService.GetRolesOfUserAsync("email");

            //Assert
            Assert.Equal(userResult, roleNames);
        }

        [Fact]
        public async Task WhenGetUserByEmailIfExistsAsyncWithInvalidUserId_ThenReturnDefault()
        {
            //Arrange
            var userEmail = "invalid";
            var roles = new List<RoleModel>();
            var roleNames = new List<string>();

            fixture.UserRepositoryMock.Setup(s => s.GetRolesOfUserAsync(It.IsAny<string>()))
                .ReturnsAsync(roles);

            //Act
            var userResult = await fixture.UserService.GetRolesOfUserAsync(userEmail);

            //Assert
            Assert.Equal(userResult, roleNames);
        }

        [Fact]
        public async Task WhenCanUserLoginAsyncWithInvalidData_ThenReturnFalse()
        {
            //Arrange
            var email = "invalid";
            var passwordTest = PasswordHasher.HashPssword("invalid");
            var password = PasswordHasher.HashPssword("valid");
            fixture.UserRepositoryMock.Setup(s => s.GetUserPasswordByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(password);

            //Act
            var result = await this.fixture.UserService.CanUserLoginAsync(email, passwordTest);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public async Task WhenCanUserLoginAsyncWithValidData_ThenReturnTrue()
        {
            //Arrange
            var email = "valid";
            var password = PasswordHasher.HashPssword("valid");
            fixture.UserRepositoryMock.Setup(s => s.GetUserPasswordByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(password);

            //Act
            var result = await this.fixture.UserService.CanUserLoginAsync(email, password);

            //Assert
            Assert.False(result);
        }
    }
}

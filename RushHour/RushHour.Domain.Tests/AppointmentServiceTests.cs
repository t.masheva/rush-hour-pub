using Moq;
using RushHour.Domain.Tests.Fixtures;
using RushHour.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Domain.Tests
{
    public class AppointmentServiceTests : IClassFixture<AppointmentServiceFixture>
    {
        private readonly AppointmentServiceFixture fixture;
        public AppointmentServiceTests(AppointmentServiceFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenAddAsync_ThenReturnCreatedAppointment()
        {
            //Arange
            var appointmentModel = new AppointmentModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                UserId = 1
            };

            fixture.AppointmentRepositoryMock.Setup(s => s.AddAsync(It.IsAny<AppointmentModel>()))
                .ReturnsAsync(new AppointmentModel
                {
                    AppointmentActivitiyModels = appointmentModel.AppointmentActivitiyModels,
                    EndDate = appointmentModel.EndDate,
                    StartDate = appointmentModel.StartDate,
                    UserId = appointmentModel.UserId,
                    Id = 1
                });

            //Act
            var result = await fixture.AppointmentService.AddAsync(appointmentModel);

            //Assert
            Assert.Equal(appointmentModel.StartDate, result.StartDate);
            Assert.Equal(appointmentModel.EndDate, result.EndDate);
            Assert.Equal(appointmentModel.UserId, result.UserId);
            Assert.Equal(appointmentModel.AppointmentActivitiyModels, result.AppointmentActivitiyModels);
        }

        [Fact]
        public async Task WhenAddAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.AppointmentService.AddAsync(null));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithNonexistentId_ThrowsArgumentException()
        {
            //Arrange
            var appointment = (AppointmentModel)default;
            fixture.AppointmentRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentService.DeleteAsync(1));
        }

        [Fact]
        public async Task WhenDeleteAsyncWithCorrectId_ThenCallDeleteAsyncInRepository()
        {
            //Arrange
            var appointment = new AppointmentModel();
            fixture.AppointmentRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);

            //Act
            await fixture.AppointmentService.DeleteAsync(1);

            //Assert
            fixture.AppointmentRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task WheGetAllAsyncWithCorrectId_ThenCallGetAllAsyncInRepository()
        {
            //Act
            await fixture.AppointmentService.GetAllAsync();

            //Assert
            fixture.AppointmentRepositoryMock.Verify(x => x.GetAllAsync(), Times.Once);
        }

        [Fact]
        public async Task WhenGetAppointmentsOfUserAsyncWithInvalidUserId_ThenThrowArgumentException()
        {
            //Arrange
            fixture.UserServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync((UserModel)default);

            //Assert
            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.AppointmentService.GetAppointmentsOfUserAsync(1));
        }

        [Fact]
        public async Task WhenGetAppointmentsOfUserAsyncWithValidUserId_ThenReturnAppointments()
        {
            //Arrange
            var userId = 1;
            var userModel = new UserModel
            {
                Id = userId
            };

            var appointments = (IEnumerable<AppointmentModel>) new List<AppointmentModel>
            {
                new AppointmentModel(),
                new AppointmentModel(),
                new AppointmentModel()
            };

            fixture.UserServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(userModel);

            fixture.AppointmentRepositoryMock.Setup(s => s.GetAppointmentsOfUserAsync(It.IsAny<int>()))
                .ReturnsAsync(appointments);

            //Act
            var appointmentsResult = await fixture.AppointmentService.GetAppointmentsOfUserAsync(userId);

            //Assert
            Assert.Equal(appointmentsResult, appointments);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithValidUserId_ThenReturnAppointment()
        {
            //Arrange
            var appointmentId = 1;
            var appointment = new AppointmentModel
            {
                Id = appointmentId,
                EndDate = DateTime.Now,
                StartDate = DateTime.Now,
                UserId = 1
            };

            fixture.AppointmentRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);

            //Act
            var appointmentResult = await fixture.AppointmentService.GetByIdIfExistsAsync(appointmentId);

            //Assert
            Assert.Equal(appointmentResult, appointment);
        }

        [Fact]
        public async Task WhenGetByIdIfExistsAsyncWithInvalidUserId_ThenReturnDefault()
        {
            //Arrange
            var appointmentId = -101;
            var appointment = (AppointmentModel)default;

            fixture.AppointmentRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);

            //Act
            var appointmentResult = await fixture.AppointmentService.GetByIdIfExistsAsync(appointmentId);

            //Assert
            Assert.Equal(appointmentResult, appointment);
        }

        [Fact]
        public async Task WhenUpdateAsync_ThenReturnEditAppointment()
        {
            //Arrange
            var appointmentModel = new AppointmentModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                UserId = 1
            };
            fixture.AppointmentRepositoryMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentModel);

            //Act
            await fixture.AppointmentService.UpdateAsync(appointmentModel);

            //Assert
            fixture.AppointmentRepositoryMock.Verify(x => x.UpdateAsync(appointmentModel), Times.Once);
        }

        [Fact]
        public async Task WhenUpdateAsyncWithNull_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await fixture.AppointmentService.UpdateAsync(null));
        }
    }
}

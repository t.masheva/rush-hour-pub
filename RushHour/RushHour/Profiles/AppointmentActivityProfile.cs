﻿using AutoMapper;
using RushHour.Entities;
using RushHour.DomainModels;

namespace RushHour.Profiles
{
    public class AppointmentActivityProfile : Profile
    {
        public AppointmentActivityProfile()
        {
            CreateMap<AppointmentActivityModel, AppointmentActivity>()
                .ForMember(dest => dest.Activity, opt => opt.MapFrom(src => src.AcrivityModel))
                .ForMember(dest => dest.Appointment, opt => opt.MapFrom(src => src.AppointmentModel));

            CreateMap<AppointmentActivity, AppointmentActivityModel>()
                .ForMember(dest => dest.AcrivityModel, opt => opt.MapFrom(src => src.Activity))
                .ForMember(dest => dest.AppointmentModel, opt => opt.MapFrom(src => src.Appointment));
        }
    }
}

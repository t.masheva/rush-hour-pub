﻿using AutoMapper;
using RushHour.Entities;
using RushHour.DomainModels;

namespace RushHour.Profiles
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleModel>();
            CreateMap<RoleModel, Role>();
        }
    }
}

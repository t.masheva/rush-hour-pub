﻿using AutoMapper;
using RushHour.Entities;
using RushHour.Models.CreateModels;
using RushHour.DomainModels;

namespace RushHours.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserModel, UserCreateModel>();
            CreateMap<UserCreateModel, UserModel>();
            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>();
        }
    }
}

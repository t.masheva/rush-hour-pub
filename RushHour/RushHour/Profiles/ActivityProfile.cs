﻿using AutoMapper;
using RushHour.Entities;
using RushHour.Models.CreateModels;
using RushHour.DomainModels;
using RushHour.Models.ResponseModels;

namespace RushHour.Profiles
{
    public class ActivityProfile : Profile
    {
        public ActivityProfile()
        {
            CreateMap<ActivityCreateModel, ActivityModel>();
            CreateMap<ActivityModel, Activity>();
            CreateMap<Activity, ActivityModel>();
            CreateMap<ActivityModel, ActivityResponseModel>();
        }
    }
}

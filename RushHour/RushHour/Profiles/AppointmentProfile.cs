﻿using AutoMapper;
using RushHour.Entities;
using RushHour.Models.CreateModels;
using RushHour.DomainModels;
using RushHour.Models.ResponseModels;
using System.Linq;
using System.Collections.Generic;

namespace RushHour.Profiles
{
    public class AppointmentProfile : Profile
    {
        public AppointmentProfile()
        {
            CreateMap<AppointmentCreateModel, AppointmentModel>();

            CreateMap<AppointmentModel, Appointment>()
                .ForMember(dest => dest.AppointmentActivities, opt => opt.MapFrom(src => src.AppointmentActivitiyModels));

            CreateMap<Appointment, AppointmentModel>()
                .ForMember(dest => dest.AppointmentActivitiyModels, opt => opt.MapFrom(src => src.AppointmentActivities));

            CreateMap<AppointmentModel, AppointmentResponseModel>()
                .ForMember(dest => dest.Activities,
                opt => opt.MapFrom(src => src.AppointmentActivitiyModels.Select(a => a.AcrivityModel)));
        }
    }
}

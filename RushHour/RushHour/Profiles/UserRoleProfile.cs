﻿using AutoMapper;
using RushHour.Entities;
using RushHour.DomainModels;

namespace RushHour.Profiles
{
    public class UserRoleProfile : Profile
    {
        public UserRoleProfile()
        {
            CreateMap<UserRoleModel, UserRole>();
            CreateMap<UserRole, UserRoleModel>();
        }
    }
}

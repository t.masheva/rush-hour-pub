﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RushHour.Domain.Interfaces;
using RushHour.Models.CreateModels;
using RushHour.DomainModels;
using RushHour.Models.Enums;
using RushHour.Models.ResponseModels;

namespace RushHour.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user, admin")]
    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentService appointmentService;
        private readonly IUserService userService;
        private readonly IActivityService activityService;
        private readonly IAppointmentActivityService appointmentActivityService;
        private readonly IMapper mapper;

        public AppointmentController(IAppointmentService appointmentService, IMapper mapper, IUserService userService, IActivityService activityService, IAppointmentActivityService appointmentActivityService)
        {
            this.appointmentService = appointmentService;
            this.mapper = mapper;
            this.userService = userService;
            this.activityService = activityService;
            this.appointmentActivityService = appointmentActivityService;
        }

        // GET: api/<AppointmentController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            if (HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.Role)
                .Any(r => r.Value == RoleEnum.admin.ToString()))
            {
                return Ok(mapper.Map<IEnumerable<AppointmentResponseModel>>(await this.appointmentService.GetAllAsync()));
            }

            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub)?.Value;
            var userId = (await this.userService.GetUserByEmailIfExistsAsync(email)).Id;

            return Ok(mapper.Map<IEnumerable<AppointmentResponseModel>>
                (await this.appointmentService.GetAppointmentsOfUserAsync(userId)));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var appointment = await this.appointmentService.GetByIdIfExistsAsync(id);

            if (appointment == default)
            {
                return BadRequest($"Appointment with this id does not exist: {id}");
            }

            if (!await IsAuthorizedAsync(appointment.UserId))
            {
                return Unauthorized();
            }

            var mappedAppointment = mapper.Map<AppointmentResponseModel>(appointment);

            return Ok(mappedAppointment);
        }

        // POST api/<AppointmentController>
        [HttpPost]
        public async Task<IActionResult> Post(AppointmentCreateModel appointmentCreateModel)
        {
            if (!await activityService.AllNamesExistAsync(appointmentCreateModel.ActivityNames))
            {
                return BadRequest($"Not all activities are valid.");
            }

            var appointment = this.mapper.Map<AppointmentModel>(appointmentCreateModel);

            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub)?.Value;
            var userId = (await this.userService.GetUserByEmailIfExistsAsync(email)).Id;

            appointment.UserId = userId;
            appointment = await this.appointmentService.AddAsync(appointment);

            await AddAppointmentActivitiesToDbAsync(appointmentCreateModel.ActivityNames, appointment.Id);

            var mappedAppointment = mapper.Map<AppointmentResponseModel>
                (await this.appointmentService.GetByIdIfExistsAsync(appointment.Id));

            return Created($"/{appointment.Id}", mappedAppointment);
        }

        private async Task AddAppointmentActivitiesToDbAsync(ICollection<string> activityNames, int appointmentId)
        {
            foreach (var name in activityNames.Distinct())
            {
                var ActivityId = (await activityService.GetByNameIfExistsAsync(name)).Id;

                await appointmentActivityService.AddAsync(new AppointmentActivityModel
                {
                    ActivityId = ActivityId,
                    AppointmentId = appointmentId
                });
            }
        }

        // PUT api/<AppointmentController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, AppointmentCreateModel appointmentCreateModel)
        {
            var appointment = await this.appointmentService.GetByIdIfExistsAsync(id);

            if (appointment == default)
            {
                return BadRequest($"Appointment with this id does not exist: {id}");
            }

            if (!await IsAuthorizedAsync(appointment.UserId))
            {
                return Unauthorized();
            }

            if (!await activityService.AllNamesExistAsync(appointmentCreateModel.ActivityNames))
            {
                return BadRequest($"Not all activities are valid.");
            }

            await this.appointmentActivityService.DeleteActivitiesOfAppointmentAsync(appointment.Id);

            await AddAppointmentActivitiesToDbAsync(appointmentCreateModel.ActivityNames, appointment.Id);

            var mappedAppointment = mapper.Map(appointmentCreateModel, appointment);
            await this.appointmentService.UpdateAsync(mappedAppointment);

            return Ok();
        }

        private async Task<bool> IsAuthorizedAsync(int appointmentUserId)
        {
            var email = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;
            var userId = (await this.userService.GetUserByEmailIfExistsAsync(email)).Id;

            if (!HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.Role)
                .Any(r => r.Value == RoleEnum.admin.ToString()) && appointmentUserId != userId)
            {
                return false;
            }

            return true;
        }

        // DELETE api/<AppointmentController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var appointment = await this.appointmentService.GetByIdIfExistsAsync(id);

            if (appointment == default)
            {
                return BadRequest($"Appointment with this id does not exist: {id}");
            }

            if (!await IsAuthorizedAsync(appointment.UserId))
            {
                return Unauthorized();
            }

            await this.appointmentActivityService.DeleteActivitiesOfAppointmentAsync(id);
            await this.appointmentService.DeleteAsync(id);

            return Ok();
        }
    }
}

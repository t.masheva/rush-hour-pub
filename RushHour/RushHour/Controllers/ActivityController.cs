﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Domain.Interfaces;
using RushHour.Models.CreateModels;
using RushHour.DomainModels;
using RushHour.Models.ResponseModels;

namespace RushHour.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
    public class ActivityController : ControllerBase
    {
        private readonly IActivityService activityService;
        private readonly IAppointmentActivityService appointmentActivityService;
        private readonly IMapper mapper;

        public ActivityController(IActivityService activityService, IMapper mapper, IAppointmentActivityService appointmentActivityService)
        {
            this.activityService = activityService;
            this.mapper = mapper;
            this.appointmentActivityService = appointmentActivityService;
        }

        // GET: api/<ActivityController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var activities = await this.activityService.GetAllAsync();

            var mappedActivities = mapper.Map<IEnumerable<ActivityResponseModel>>(activities);

            return Ok(mappedActivities);
        }

        // GET api/<ActivityController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var activity = await this.activityService.GetByIdIfExistsAsync(id);

            if (activity == default)
            {
                return BadRequest($"Activity with this id does not exist: {id}");
            }

            var mappedActivity = mapper.Map<ActivityResponseModel>(activity);

            return Ok(mappedActivity);
        }

        // POST api/<ActivityController>
        [HttpPost]
        public async Task<IActionResult> Post(ActivityCreateModel activityCreateModel)
        {
            var activity = this.mapper.Map<ActivityCreateModel, ActivityModel>(activityCreateModel);

            activity = await this.activityService.AddAsync(activity);
            var mappedActivity = mapper.Map<ActivityResponseModel>(activity);

            return Created($"/{activity.Id}", mappedActivity);
        }

        // PUT api/<ActivityController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, ActivityCreateModel activityCreateModel)
        {
            var activity = await this.activityService.GetByIdIfExistsAsync(id);

            if (activity == default)
            {
                return BadRequest($"Activity with this id does not exist: {id}");
            }

            var mappedActivity = mapper.Map(activityCreateModel, activity);

            await this.activityService.UpdateAsync(mappedActivity);

            return Ok();
        }

        // DELETE api/<ActivityController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var activity = await this.activityService.GetByIdIfExistsAsync(id);

            if (activity == default)
            {
                return BadRequest($"Activity with this id does not exist: {id}");
            }

            await this.appointmentActivityService.DeleteAppointmentsOfActivityAsync(id);
            await this.activityService.DeleteAsync(id);

            return Ok();
        }
    }
}

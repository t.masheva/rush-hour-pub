﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Domain.Interfaces;
using RushHour.Helpers;
using RushHour.Models.CreateModels;
using RushHour.DomainModels;
using RushHour.Models.Enums;

namespace RushHour.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ISecurityService securityService;
        private readonly IUserService userService;
        private readonly IRoleService roleService;
        private readonly IUserRoleService userRoleService;
        private readonly IMapper mapper;

        public UserController(ISecurityService securityService, IUserService userService, IRoleService roleService, IUserRoleService userRoleService ,IMapper mapper)
        {
            this.securityService = securityService;
            this.userService = userService;
            this.roleService = roleService;
            this.userRoleService = userRoleService;
            this.mapper = mapper;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string email, string password)
        {
            if (!await this.userService.CanUserLoginAsync(email, password))
            {
                return BadRequest("User credentials are invalid");
            }

            var roles = await this.userService.GetRolesOfUserAsync(email);
            var token = securityService.GenerateJwtToken(email, roles);

            return Ok(token);
        }


        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] UserCreateModel userCreateModel)
        {
            var hashedPassword = PasswordHasher.HashPssword(userCreateModel.Password);
            userCreateModel.Password = hashedPassword;

            if (await this.userService.GetUserByEmailIfExistsAsync(userCreateModel.Email) != default)
            {
                return BadRequest($"User with this email already exists: {userCreateModel.Email}");
            }

            var userModel = this.mapper.Map<UserModel>(userCreateModel);
            userModel = await this.userService.AddAsync(userModel);

            await AddUserRolesToDbAsync(userModel);

            var roles = await this.userService.GetRolesOfUserAsync(userCreateModel.Email);
            var token = securityService.GenerateJwtToken(userCreateModel.Email, roles);

            return Ok(token);
        }

        private async Task AddUserRolesToDbAsync(UserModel userModel)
        {
            var role = await roleService.AddAsync(new RoleModel()
            {
                Name = RoleEnum.user.ToString()
            });

            await userRoleService.AddAsync(new UserRoleModel
            {
                RoleId = role.Id,
                UserId = userModel.Id
            });

            if (userModel.Email == $"{RoleEnum.admin}@mail.com")
            {
                role = await roleService.AddAsync(new RoleModel()
                {
                    Name = RoleEnum.admin.ToString()
                });

                await userRoleService.AddAsync(new UserRoleModel
                {
                    RoleId = role.Id,
                    UserId = userModel.Id
                });
            }
        }
    }
}

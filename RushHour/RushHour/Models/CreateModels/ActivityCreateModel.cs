﻿using System.ComponentModel.DataAnnotations;

namespace RushHour.Models.CreateModels
{
    public class ActivityCreateModel
    {
        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        [Range(0.01, 999999999, ErrorMessage = "Price must be greater than 0.00")]
        public decimal Price { get; set; }

        [Required]
        [Range(0.01, 999999999, ErrorMessage = "Duration must be greater than 0")]
        public decimal Duration { get; set; }
    }
}

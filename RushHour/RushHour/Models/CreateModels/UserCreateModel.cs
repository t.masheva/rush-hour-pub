﻿using System.ComponentModel.DataAnnotations;

namespace RushHour.Models.CreateModels
{
    public class UserCreateModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

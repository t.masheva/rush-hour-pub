﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Models.CreateModels
{
    public class AppointmentCreateModel
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public ICollection<string> ActivityNames { get; set; }
    }
}

﻿namespace RushHour.Models.ResponseModels
{
    public class ActivityResponseModel
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Duration { get; set; }
    }
}

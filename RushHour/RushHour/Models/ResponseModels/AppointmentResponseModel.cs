﻿using RushHour.DomainModels;
using System;
using System.Collections.Generic;

namespace RushHour.Models.ResponseModels
{
    public class AppointmentResponseModel
    {
        public AppointmentResponseModel()
        {
            this.Activities = new List<ActivityModel>();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IList<ActivityModel> Activities { get; set; }
    }
}

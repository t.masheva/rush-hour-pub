﻿using System.Collections.Generic;

namespace RushHour.Entities
{
    public class Role : BaseEntity
    {
        public Role()
        {
            this.UserRoles = new HashSet<UserRole>();
        }

        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}

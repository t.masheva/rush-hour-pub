﻿using System.Collections.Generic;

namespace RushHour.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            this.UserRoles = new HashSet<UserRole>();
            this.Appointments = new HashSet<Appointment>();
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}

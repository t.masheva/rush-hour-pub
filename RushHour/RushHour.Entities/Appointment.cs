﻿using System;
using System.Collections.Generic;

namespace RushHour.Entities
{
    public class Appointment : BaseEntity
    {
        public Appointment()
        {
            this.AppointmentActivities = new HashSet<AppointmentActivity>();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<AppointmentActivity> AppointmentActivities { get; set; }
    }
}

﻿namespace RushHour.Entities
{
    public class AppointmentActivity : BaseEntity
    {
        public int ActivityId { get; set; }

        public virtual Activity Activity { get; set; }

        public int AppointmentId { get; set; }

        public virtual Appointment Appointment { get; set; }
    }
}

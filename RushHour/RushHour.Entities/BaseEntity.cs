﻿
using System.ComponentModel.DataAnnotations;

namespace RushHour.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace RushHour.Entities
{
    public class Activity : BaseEntity
    {
        public Activity()
        {
            this.AppointmentActivities = new HashSet<AppointmentActivity>();
        }
        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Duration { get; set; }

        public virtual ICollection<AppointmentActivity> AppointmentActivities { get; set; }
    }
}

﻿namespace RushHour.DomainModels
{
    public class UserRoleModel : BaseModel
    {
        public int UserId { get; set; }

        public int RoleId { get; set; }
    }
}

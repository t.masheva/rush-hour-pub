﻿using System;
using System.Collections.Generic;

namespace RushHour.DomainModels
{
    public class AppointmentModel : BaseModel
    {
        public AppointmentModel()
        {
            this.AppointmentActivitiyModels = new List<AppointmentActivityModel>();
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int UserId { get; set; }
        public List<AppointmentActivityModel> AppointmentActivitiyModels { get; set; }
    }
}

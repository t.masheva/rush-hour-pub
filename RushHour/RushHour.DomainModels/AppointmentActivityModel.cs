﻿namespace RushHour.DomainModels
{
    public class AppointmentActivityModel : BaseModel
    {
        public int ActivityId { get; set; }

        public ActivityModel AcrivityModel { get; set; }

        public int AppointmentId { get; set; }

        public AppointmentModel AppointmentModel { get; set; }
    }
}

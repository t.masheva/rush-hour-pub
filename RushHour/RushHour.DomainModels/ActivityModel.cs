﻿namespace RushHour.DomainModels
{
    public class ActivityModel : BaseModel
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Duration { get; set; }
    }
}

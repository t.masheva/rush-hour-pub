﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using RushHour.Controllers.Tests.Fixtures;
using RushHour.DomainModels;
using RushHour.Models.CreateModels;
using RushHour.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Controllers.Tests
{
    public class UserControllerTests : IClassFixture<UserControllerFixture>
    {
        private readonly UserControllerFixture fixture;
        public UserControllerTests(UserControllerFixture fixture)
            => this.fixture = fixture;

        [Theory]
        [InlineData("", "")]
        [InlineData("invalid", "invalid")]
        [InlineData("invalid", "valid")]
        [InlineData("valid", "invalid")]
        public async Task WhenLoginWithInvalidData_ThenReturnBadRequest(string email, string password)
        {
            //Arrange
            fixture.UserServiceMock.Setup(s => s.CanUserLoginAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(false);

            //Act
            var result = (BadRequestObjectResult)await fixture.UserController.Login(email, password);
            var expected = new BadRequestObjectResult($"User credentials are invalid");

            //Assert
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Theory]
        [InlineData("email", "password")]
        public async Task WhenLoginWithValidData_ThenReturnOkWithToken(string email, string password)
        {
            //Arrange
            var token = "string-token";
            fixture.UserServiceMock.Setup(s => s.CanUserLoginAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(true);
            fixture.UserServiceMock.Setup(s => s.GetRolesOfUserAsync(It.IsAny<string>()))
                .ReturnsAsync(new List<string> { RoleEnum.admin.ToString(), RoleEnum.user.ToString() });
            fixture.SecurityServiceMock.Setup(s => s.GenerateJwtToken(It.IsAny<string>(), It.IsAny<IEnumerable<string>>()))
                .Returns(token);

            //Act
            var result = (OkObjectResult)await fixture.UserController.Login(email, password);
            var expected = new OkObjectResult(token);

            //Assert
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenRegisterWithExistingEmail_ThenReturnBadRequestResult()
        {
            //Arrange
            var userCreateModel = new UserCreateModel()
            {
                Email = "email",
                Password = "password",
                FirstName = "John",
                LastName = "Smith"
            };

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync((UserModel)default);

            //Act
            var result = (BadRequestObjectResult)await fixture.UserController.Register(userCreateModel);
            var expected = new BadRequestObjectResult($"User with this email already exists: {userCreateModel.Email}");

            //Assert
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }


        [Fact]
        public async Task WhenRegisterWithNonexistentgEmail_ThenReturnOkResult()
        {
            //Arrange
            var token = "string-token";
            var userCreateModel = new UserCreateModel()
            {
                Email = "admin@mail.com",
                Password = "password",
                FirstName = "John",
                LastName = "Smith"
            };
            var userModel = new UserModel()
            {
                Email = userCreateModel.Email,
                FirstName = userCreateModel.FirstName,
                LastName = userCreateModel.LastName,
                Password = "hashed password"
            };
            var role = new RoleModel()
            {
                Name = RoleEnum.admin.ToString(),
                Id = 1
            };

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(userModel);

            fixture.MapperMock.Setup(s => s.Map<UserModel>(It.IsAny<UserCreateModel>()))
                .Returns(userModel);

            userModel.Id = 1;

            fixture.UserServiceMock.Setup(s => s.AddAsync(It.IsAny<UserModel>()))
                .ReturnsAsync(userModel);

            fixture.RoleServiceMock.Setup(s => s.AddAsync(It.IsAny<RoleModel>()))
                .ReturnsAsync(role);
            fixture.UserRoleServiceMock.Setup(s => s.AddAsync(It.IsAny<UserRoleModel>()))
                .ReturnsAsync(new UserRoleModel { 
                    RoleId = role.Id,
                    UserId =  userModel.Id,
                    Id = 1
                });

            fixture.UserServiceMock.Setup(s => s.GetRolesOfUserAsync(It.IsAny<string>()))
                .ReturnsAsync(new List<string> { RoleEnum.admin.ToString(), RoleEnum.user.ToString() });

            fixture.SecurityServiceMock.Setup(s => s.GenerateJwtToken(It.IsAny<string>(), It.IsAny<IEnumerable<string>>()))
                .Returns(token);

            //Act
            var result = (OkObjectResult)await fixture.UserController.Register(userCreateModel);
            var expected = new OkObjectResult(token);

            //Assert
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }
    }
}

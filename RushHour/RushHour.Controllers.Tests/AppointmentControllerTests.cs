using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Models.CreateModels;
using RushHour.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using RushHour.Models.Enums;
using RushHour.Controllers.Tests.Fixtures;

namespace RushHour.Controllers.Tests
{
    public class AppointmentControllerTests : IClassFixture<AppointmentControllerFixture>
    {
        private readonly AppointmentControllerFixture fixture;
        public AppointmentControllerTests(AppointmentControllerFixture fixture)
            => this.fixture = fixture;

        [Theory]
        [InlineData(RoleEnum.user)]
        [InlineData(RoleEnum.admin)]
        public async Task WhenGetAll_ThenSuccessfullyReturnAppointments(RoleEnum roleEnum)
        {
            var appointmentModels = new List<AppointmentModel>();
            var appointmentResponseModels = new List<AppointmentResponseModel>();            

            for (int i = 0; i < 5; i++)
            {
                appointmentModels.Add(new AppointmentModel
                {
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(i),
                    Id = i + 1,
                    UserId = 1,
                    AppointmentActivitiyModels = new List<AppointmentActivityModel>()
                });

                appointmentResponseModels.Add(new AppointmentResponseModel
                {
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(i),
                    Activities = new List<ActivityModel>()
                });
            }

            fixture.AppointmentServiceMock.Setup(s => s.GetAllAsync())
                .ReturnsAsync((IEnumerable<AppointmentModel>)appointmentModels);

            fixture.MapperMock.Setup(s => s.Map<IEnumerable<AppointmentResponseModel>>(It.IsAny<IEnumerable<AppointmentModel>>()))
                .Returns(appointmentResponseModels);

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new UserModel
                {
                    Id = 1
                });

            fixture.ChangeRoleEnum(roleEnum);

            var result = await fixture.AppointmentController.Get();
            var createdResult = result as OkObjectResult;
            var expected = new OkObjectResult(appointmentResponseModels);

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
            Assert.Equal(expected.Value, createdResult.Value);
        }

        [Fact]
        public async Task WhenGetByIdWithInvalidAppointmentId_ThenReturnbadRequestResult()
        {
            int id = -10;

            ArrangeInvalidUserId_ShouldReturnBadRequestResult();

            var result = (BadRequestObjectResult)await fixture.AppointmentController.Get(id);
            var expected = new BadRequestObjectResult($"Appointment with this id does not exist: {id}");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenGetByIdWithInvalidUserId_ThenReturnUnauthorizedResult()
        {
            int id = 1;

            ArrangeInvaidUser_ShouldReturnUnauthorized(id);

            var result = await fixture.AppointmentController.Get(id);
            var createdResult = result as UnauthorizedResult;
            var expected = new UnauthorizedResult();

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
        }

        [Theory]
        [InlineData(1, 1, RoleEnum.user)]
        [InlineData(1, 2, RoleEnum.admin)]
        public async Task WhenGetByIdWithCorrectData_ThenReturnOkWithAppointment(int userId, int creatorId, RoleEnum roleEnum)
        {
            var activityName = "string";
            var appointmentId = 1;
            var ActivityId = 1;

            AppointmentModel appointmentModel = new AppointmentModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                UserId = creatorId,
                Id = appointmentId,
                AppointmentActivitiyModels = new List<AppointmentActivityModel>
                {
                    new AppointmentActivityModel
                    {
                        AppointmentId = appointmentId,
                        ActivityId = ActivityId
                    }
                }
            };

            AppointmentResponseModel appointmentResponseModel = new AppointmentResponseModel()
            {
                StartDate = appointmentModel.StartDate,
                EndDate = appointmentModel.EndDate,
                Activities = new List<ActivityModel>
                {
                    new ActivityModel
                    {
                        Name = activityName,
                        Id = ActivityId
                    }
                }
            };

            fixture.AppointmentServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentModel);

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new UserModel
                {
                    Id = userId
                });

            fixture.MapperMock.Setup(s => s.Map<AppointmentResponseModel>(It.IsAny<AppointmentModel>()))
                .Returns(appointmentResponseModel);

            fixture.ChangeRoleEnum(roleEnum);

            var result = await fixture.AppointmentController.Get(appointmentId);
            var createdResult = result as OkObjectResult;
            var expected = new OkObjectResult(appointmentResponseModel);

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
            Assert.Equal(expected.Value, createdResult.Value);
        }

        [Fact]
        public async Task WhenPostWithInvalidActivityNames_ThenReturnBadRequestResult()
        {
            var appointmentCreateModel = new AppointmentCreateModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                ActivityNames = new string[] { "string" }
            };

            fixture.ActivityServiceMock.Setup(s => s.AllNamesExistAsync(It.IsAny<ICollection<string>>()))
                .ReturnsAsync(false);

            var result = (BadRequestObjectResult)await fixture.AppointmentController.Post(appointmentCreateModel);
            var expected = new BadRequestObjectResult($"Not all activities are valid.");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenPostWithCorrectData_ThenSuccessfullyCreateAppointment()
        {
            var userId = 1;
            var activityName = "string";
            var appointmentId = 1;
            var ActivityId = 1;

            AppointmentCreateModel appointmentCreateModel = new AppointmentCreateModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                ActivityNames = new string[] { activityName }
            };

            AppointmentResponseModel appointmentResponseModel = new AppointmentResponseModel()
            {
                StartDate = appointmentCreateModel.StartDate,
                EndDate = appointmentCreateModel.EndDate,
                Activities = new List<ActivityModel>
                {
                    new ActivityModel
                    {
                        Name = activityName,
                        Id = ActivityId
                    }
                }
            };

            fixture.AppointmentServiceMock.Setup(s => s.AddAsync(It.IsAny<AppointmentModel>()))
                .ReturnsAsync(new AppointmentModel
                {
                    Id = appointmentId,
                    StartDate = appointmentCreateModel.StartDate,
                    EndDate = appointmentCreateModel.EndDate,
                    UserId = userId,
                    AppointmentActivitiyModels = new List<AppointmentActivityModel>
                {
                    new AppointmentActivityModel
                    {
                        AppointmentId = appointmentId,
                        ActivityId = ActivityId
                    }
                }
                });

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new UserModel
                {
                    Id = userId
                });

            fixture.ActivityServiceMock.Setup(s => s.AllNamesExistAsync(It.IsAny<ICollection<string>>()))
                .ReturnsAsync(true);
            fixture.ActivityServiceMock.Setup(s => s.GetByNameIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new ActivityModel
                {
                    Name = activityName,
                    Id = ActivityId
                });

            fixture.MapperMock.Setup(s => s.Map<AppointmentModel>(It.IsAny<AppointmentCreateModel>()))
                .Returns(new AppointmentModel
                {
                    StartDate = appointmentCreateModel.StartDate,
                    EndDate = appointmentCreateModel.EndDate,
                    UserId = userId
                });

            fixture.MapperMock.Setup(s => s.Map<AppointmentResponseModel>(It.IsAny<AppointmentModel>()))
                .Returns(appointmentResponseModel);

            var result = await fixture.AppointmentController.Post(appointmentCreateModel);
            var createdResult = result as CreatedResult;
            var expected = new CreatedResult($"/{appointmentId}", appointmentResponseModel);

            Assert.NotNull(result);
            Assert.Equal(expected.Location, createdResult.Location);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
            Assert.Equal(expected.Value, createdResult.Value);
        }

        [Fact]
        public async Task WhenPutWithInvalidAppointmentId_ThenReturnBadRequestResult()
        {
            int id = -10;

            ArrangeInvalidUserId_ShouldReturnBadRequestResult();

            var result = (BadRequestObjectResult)await fixture.AppointmentController.Put(id, null);
            var expected = new BadRequestObjectResult($"Appointment with this id does not exist: {id}");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenPutWithInvalidUserId_ThenReturnUnauthorizedResult()
        {
            int id = 1;

            ArrangeInvaidUser_ShouldReturnUnauthorized(id);

            var result = await fixture.AppointmentController.Put(id, null);
            var createdResult = result as UnauthorizedResult;
            var expected = new UnauthorizedResult();

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
        }

        [Fact]
        public async Task WhenPutWithInvalidActivityNames_ThenReturnBadRequestResult()
        {
            var userId = 1;
            int id = 1;
            var activityName = "string";

            AppointmentModel appointment = new AppointmentModel
            {
                UserId = userId,
                Id = id
            };

            var appointmentCreateModel = new AppointmentCreateModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                ActivityNames = new string[] { activityName }
            };

            fixture.AppointmentServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new UserModel
                {
                    Id = userId
                });

            fixture.ActivityServiceMock.Setup(s => s.AllNamesExistAsync(It.IsAny<ICollection<string>>()))
                .ReturnsAsync(false);

            var result = (BadRequestObjectResult)await fixture.AppointmentController.Put(id, appointmentCreateModel);
            var expected = new BadRequestObjectResult($"Not all activities are valid.");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Theory]
        [InlineData(1, 1, RoleEnum.user)]
        [InlineData(1, 2, RoleEnum.admin)]
        public async Task WhenPutWithCorrectData_ThenSuccessfullyUpdateAppointment(int userId, int creatorId, RoleEnum roleEnum)
        {
            var activityName = "string";
            var appointmentId = 1;
            var ActivityId = 1;

            AppointmentModel appointmentModel = new AppointmentModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                UserId = userId,
                Id = appointmentId,
                AppointmentActivitiyModels = new List<AppointmentActivityModel>
                {
                    new AppointmentActivityModel
                    {
                        AppointmentId = appointmentId,
                        ActivityId = ActivityId
                    }
                }
            };

            AppointmentCreateModel appointmentCreateModel = new AppointmentCreateModel
            {
                StartDate = appointmentModel.StartDate.AddDays(5),
                EndDate = appointmentModel.EndDate.AddDays(5),
                ActivityNames = new string[] { activityName }
            };

            fixture.ActivityServiceMock.Setup(s => s.AllNamesExistAsync(It.IsAny<ICollection<string>>()))
                .ReturnsAsync(true);
            fixture.ActivityServiceMock.Setup(s => s.GetByNameIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new ActivityModel
                {
                    Name = activityName,
                    Id = ActivityId
                });

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new UserModel
                {
                    Id = creatorId
                });

            fixture.AppointmentServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointmentModel);

            fixture.MapperMock.Setup(s => s.Map<AppointmentModel>(It.IsAny<AppointmentCreateModel>()))
                .Returns(new AppointmentModel
                {
                    StartDate = appointmentCreateModel.StartDate,
                    EndDate = appointmentCreateModel.EndDate
                });

            fixture.ChangeRoleEnum(roleEnum);

            var result = await fixture.AppointmentController.Put(appointmentId ,appointmentCreateModel);
            var createdResult = result as OkResult;
            var expected = new OkResult();

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
        }

        [Fact]
        public async Task WhenDeleteWithInvalidAppointmentId_ThenReturnBadRequestResult()
        {
            int id = -10;

            ArrangeInvalidUserId_ShouldReturnBadRequestResult();

            var result = (BadRequestObjectResult)await fixture.AppointmentController.Delete(id);
            var expected = new BadRequestObjectResult($"Appointment with this id does not exist: {id}");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenDeleteWithInvalidUserId_ThenReturnUnauthorizedResult()
        {
            int id = 1;

            ArrangeInvaidUser_ShouldReturnUnauthorized(id);

            var result = await fixture.AppointmentController.Delete(id);
            var createdResult = result as UnauthorizedResult;
            var expected = new UnauthorizedResult();

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
        }


        [Fact]
        public async Task WhenDeleteWithValidActivityId_ThenSuccessfullyDeleteActivity()
        {
            int id = 1;

            AppointmentModel activityModel = new AppointmentModel
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(5),
                UserId = 1,
                AppointmentActivitiyModels = new List<AppointmentActivityModel>
                {
                    new AppointmentActivityModel
                    {
                        AppointmentId = 1,
                        ActivityId = 1
                    }
                },
                Id = id
            };

            fixture.AppointmentServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activityModel);

            var result = (OkResult)await fixture.AppointmentController.Delete(id);
            var expected = new OkResult();
            Assert.Equal(expected.StatusCode, result.StatusCode);
        }

        private void ArrangeInvalidUserId_ShouldReturnBadRequestResult()
        {
            AppointmentModel appointment = default;

            fixture.AppointmentServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);
        }

        private void ArrangeInvaidUser_ShouldReturnUnauthorized(int id)
        {
            AppointmentModel appointment = new AppointmentModel
            {
                UserId = 2,
                Id = id
            };

            fixture.AppointmentServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(appointment);

            fixture.UserServiceMock.Setup(s => s.GetUserByEmailIfExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(new UserModel
                {
                    Id = 1
                });

            fixture.ChangeRoleEnum(RoleEnum.user);
        }
    }
}

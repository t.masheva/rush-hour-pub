using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Models.CreateModels;
using RushHour.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using RushHour.Controllers.Tests.Fixtures;

namespace RushHour.Controllers.Tests
{
    public class ActivityControllerTests : IClassFixture<ActivityControllerFixture>
    {
        private readonly ActivityControllerFixture fixture;
        public ActivityControllerTests(ActivityControllerFixture fixture)
            => this.fixture = fixture;

        [Fact]
        public async Task WhenGetAll_ThenSuccessfullyReturnActivitys()
        {
            var activityModels = new List<ActivityModel>();
            var activityResponseModels = new List<ActivityResponseModel>();            

            for (int i = 0; i < 5; i++)
            {
                activityModels.Add(new ActivityModel
                {
                    Duration = 10.55m,
                    Name = "running",
                    Price = 20
                });

                activityResponseModels.Add(new ActivityResponseModel
                {
                    Duration = 10.55m,
                    Name = "running",
                    Price = 20
                });
            }

            fixture.ActivityServiceMock.Setup(s => s.GetAllAsync())
                .ReturnsAsync(activityModels);

            fixture.MapperMock.Setup(s => s.Map<IEnumerable<ActivityResponseModel>>(It.IsAny<IEnumerable<ActivityModel>>()))
                .Returns(activityResponseModels);

            var result = await fixture.ActivityController.Get();
            var createdResult = result as OkObjectResult;
            var expected = new OkObjectResult(activityResponseModels);

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
            Assert.Equal(expected.Value, createdResult.Value);
        }

        [Fact]
        public async Task WhenGetByIdWithInvalidActivityId_ThenReturnbadRequestResult()
        {
            int id = -10;

            ArrangeInvalidActivityId_ShouldReturnBadRequestResult();

            var result = (BadRequestObjectResult)await fixture.ActivityController.Get(id);
            var expected = new BadRequestObjectResult($"Activity with this id does not exist: {id}");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenGetByIdWithCorrectData_ThenReturnOkWithActivity()
        {
            var activityId = 1;

            ActivityModel activityModel = new ActivityModel
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20,
                Id = activityId
            };

            ActivityResponseModel activityResponseModel = new ActivityResponseModel()
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20
            };

            fixture.ActivityServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activityModel);

            fixture.MapperMock.Setup(s => s.Map<ActivityResponseModel>(It.IsAny<ActivityModel>()))
                .Returns(activityResponseModel);

            var result = await fixture.ActivityController.Get(activityId);
            var createdResult = result as OkObjectResult;
            var expected = new OkObjectResult(activityResponseModel);

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
            Assert.Equal(expected.Value, createdResult.Value);
        }

        [Fact]
        public async Task WhenPostWithCorrectData_ThenSuccessfullyCreateActivity()
        {
            var activityId = 1;

            ActivityCreateModel activityCreateModel = new ActivityCreateModel
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20
            };

            ActivityResponseModel activityResponseModel = new ActivityResponseModel()
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20
            };

            fixture.ActivityServiceMock.Setup(s => s.AddAsync(It.IsAny<ActivityModel>()))
                .ReturnsAsync(new ActivityModel
                {
                    Duration = 10.55m,
                    Name = "running",
                    Price = 20,
                    Id = activityId
                });

            fixture.MapperMock.Setup(s => s.Map<ActivityModel>(It.IsAny<ActivityCreateModel>()))
                .Returns(new ActivityModel
                {
                    Duration = 10.55m,
                    Name = "running",
                    Price = 20
                });

            fixture.MapperMock.Setup(s => s.Map<ActivityResponseModel>(It.IsAny<ActivityModel>()))
                .Returns(activityResponseModel);

            var result = await fixture.ActivityController.Post(activityCreateModel);
            var createdResult = result as CreatedResult;
            var expected = new CreatedResult($"/{activityId}", activityResponseModel);

            Assert.NotNull(result);
            Assert.Equal(expected.Location, createdResult.Location);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
            Assert.Equal(expected.Value, createdResult.Value);
        }

        [Fact]
        public async Task WhenPutWithInvalidActivityId_ThenReturnBadRequestResult()
        {
            int id = -10;

            ArrangeInvalidActivityId_ShouldReturnBadRequestResult();

            var result = (BadRequestObjectResult)await fixture.ActivityController.Put(id, null);
            var expected = new BadRequestObjectResult($"Activity with this id does not exist: {id}");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }

        [Fact]
        public async Task WhenPutWithCorrectData_ThenSuccessfullyUpdateActivity()
        {
            var activityId = 1;

            ActivityModel activityModel = new ActivityModel
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20,
                Id = activityId
            };

            ActivityCreateModel activityCreateModel = new ActivityCreateModel
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20
            };

            fixture.ActivityServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activityModel);

            fixture.MapperMock.Setup(s => s.Map<ActivityModel>(It.IsAny<ActivityCreateModel>()))
                .Returns(new ActivityModel
                {
                    Duration = 10.55m,
                    Name = "running",
                    Price = 20
                });

            var result = await fixture.ActivityController.Put(activityId ,activityCreateModel);
            var createdResult = result as OkResult;
            var expected = new OkResult();

            Assert.NotNull(result);
            Assert.Equal(expected.StatusCode, createdResult.StatusCode);
        }

        [Fact]
        public async Task WhenDeleteWithInvalidActivityId_ThenReturnBadRequestResult()
        {
            int id = -10;

            ArrangeInvalidActivityId_ShouldReturnBadRequestResult();

            var result = (BadRequestObjectResult)await fixture.ActivityController.Delete(id);
            var expected = new BadRequestObjectResult($"Activity with this id does not exist: {id}");
            Assert.Equal(expected.StatusCode, result.StatusCode);
            Assert.Equal(expected.Value, result.Value);
        }


        [Fact]
        public async Task WhenDeleteWithValidActivityId_ThenSuccessfullyDeleteActivity()
        {
            int id = 1;

            ActivityModel activityModel = new ActivityModel
            {
                Duration = 10.55m,
                Name = "running",
                Price = 20,
                Id = id
            };

            fixture.ActivityServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activityModel);

            var result = (OkResult)await fixture.ActivityController.Delete(id);
            var expected = new OkResult();
            Assert.Equal(expected.StatusCode, result.StatusCode);
        }

        private void ArrangeInvalidActivityId_ShouldReturnBadRequestResult()
        {
            ActivityModel activity = default;

            fixture.ActivityServiceMock.Setup(s => s.GetByIdIfExistsAsync(It.IsAny<int>()))
                .ReturnsAsync(activity);
        }
    }
}

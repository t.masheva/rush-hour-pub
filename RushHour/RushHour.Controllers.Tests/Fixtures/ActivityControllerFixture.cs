﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Models.Enums;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Controllers.Tests.Fixtures
{

    public class ActivityControllerFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.ActivityServiceMock = new Mock<IActivityService>();

            this.AppointmentActivityServiceMock = new Mock<IAppointmentActivityService>();

            this.MapperMock = new Mock<IMapper>();

            var controller = new ActivityController(ActivityServiceMock.Object,
                MapperMock.Object,
                AppointmentActivityServiceMock.Object);

            this.ActivityController = controller;
        }

        public Task DisposeAsync()
        {
            ActivityController = null;
            return Task.CompletedTask;
        }

        public ActivityController ActivityController { get; private set; }
        public Mock<IAppointmentActivityService> AppointmentActivityServiceMock { get; private set; }
        public Mock<IActivityService> ActivityServiceMock { get; private set; }
        public Mock<IMapper> MapperMock { get; private set; }
    }
}

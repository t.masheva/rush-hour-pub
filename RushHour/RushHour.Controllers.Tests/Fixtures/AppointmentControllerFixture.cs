﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Models.Enums;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Controllers.Tests.Fixtures
{

    public class AppointmentControllerFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.AppointmentActivityServiceMock = new Mock<IAppointmentActivityService>();

            this.ActivityServiceMock = new Mock<IActivityService>();

            this.UserServiceMock = new Mock<IUserService>();

            this.AppointmentServiceMock = new Mock<IAppointmentService>();

            this.MapperMock = new Mock<IMapper>();

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, "email"),
                new Claim(ClaimTypes.Role, RoleEnum.user.ToString())
            };

            var httpContext = Mock.Of<HttpContext>(h => h.User.Claims == claims);

            var controller = new AppointmentController(AppointmentServiceMock.Object,
                MapperMock.Object,
                UserServiceMock.Object,
                ActivityServiceMock.Object,
                AppointmentActivityServiceMock.Object)
            {
                ControllerContext = new ControllerContext()
                {
                    HttpContext = httpContext
                }
            };

            this.AppointmentController = controller;
        }

        public Task DisposeAsync()
        {
            AppointmentController = null;
            return Task.CompletedTask;
        }

        public AppointmentController AppointmentController { get; private set; }
        public Mock<IAppointmentActivityService> AppointmentActivityServiceMock { get; private set; }
        public Mock<IActivityService> ActivityServiceMock { get; private set; }
        public Mock<IUserService> UserServiceMock { get; private set; }
        public Mock<IAppointmentService> AppointmentServiceMock { get; private set; }
        public Mock<IMapper> MapperMock { get; private set; }

        internal void ChangeRoleEnum(RoleEnum roleEnum)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, "email"),
                new Claim(ClaimTypes.Role, roleEnum.ToString())
            };

            var httpContext = Mock.Of<HttpContext>(h => h.User.Claims == claims);

            this.AppointmentController.ControllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RushHour.Domain.Interfaces;
using RushHour.Models.Enums;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Controllers.Tests.Fixtures
{

    public class UserControllerFixture : IAsyncLifetime
    {
        public async Task InitializeAsync()
        {
            this.UserServiceMock = new Mock<IUserService>();
            this.RoleServiceMock = new Mock<IRoleService>();
            this.UserRoleServiceMock = new Mock<IUserRoleService>();
            this.SecurityServiceMock = new Mock<ISecurityService>();

            this.MapperMock = new Mock<IMapper>();

            var controller = new UserController(
                SecurityServiceMock.Object,
                UserServiceMock.Object,
                RoleServiceMock.Object,
                UserRoleServiceMock.Object,
                MapperMock.Object);

            this.UserController = controller;
        }

        public Task DisposeAsync()
        {
            UserController = null;
            return Task.CompletedTask;
        }

        public UserController UserController { get; private set; }
        public Mock<ISecurityService> SecurityServiceMock { get; private set; }
        public Mock<IUserService> UserServiceMock { get; private set; }
        public Mock<IRoleService> RoleServiceMock { get; private set; }
        public Mock<IUserRoleService> UserRoleServiceMock { get; private set; }
        public Mock<IMapper> MapperMock { get; private set; }
    }
}

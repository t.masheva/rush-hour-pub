﻿using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain
{
    public class ActivityService : IActivityService
    {
        private readonly IActivityRepository activityRepository;

        public ActivityService(IActivityRepository activityRepository)
        {
            this.activityRepository = activityRepository;
        }

        public async Task<ActivityModel> AddAsync(ActivityModel activityModel)
        {
            if (activityModel == default)
            {
                throw new ArgumentNullException(nameof(activityModel));
            }

            return await this.activityRepository.AddAsync(activityModel);
        }

        public async Task<bool> AllNamesExistAsync(ICollection<string> activityNames)
        {
            if (activityNames == default)
            {
                throw new ArgumentNullException(nameof(activityNames));
            }

            return await this.activityRepository.AllNamesExistAsync(activityNames);
        }

        public async Task DeleteAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"Activity with this id does not exist: {id}");
            }

            await this.activityRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<ActivityModel>> GetAllAsync()
        {
            return await this.activityRepository.GetAllAsync();
        }

        public async Task<ActivityModel> GetByIdIfExistsAsync(int id)
        {
            return await this.activityRepository.GetByIdIfExistsAsync(id);
        }

        public async Task<ActivityModel> GetByNameIfExistsAsync(string name)
        {
            return await this.activityRepository.GetByNameIfExistsAsync(name);
        }

        public async Task UpdateAsync(ActivityModel activityModel)
        {
            if (activityModel == default)
            {
                throw new ArgumentNullException(nameof(activityModel));
            }

            await this.activityRepository.UpdateAsync(activityModel);
        }
    }
}

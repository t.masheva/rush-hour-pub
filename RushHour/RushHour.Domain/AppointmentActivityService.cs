﻿using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Repositories.Interfaces;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain
{
    public class AppointmentActivityService : IAppointmentActivityService
    {
        private readonly IAppointmentActivityRepository appointmentActivityRepository;
        public AppointmentActivityService(IAppointmentActivityRepository appointmentActivityRepository)
        {
            this.appointmentActivityRepository = appointmentActivityRepository;
        }

        public async Task<AppointmentActivityModel> AddAsync(AppointmentActivityModel appointmentActivityModel)
        {
            if (appointmentActivityModel == default)
            {
                throw new ArgumentNullException(nameof(appointmentActivityModel));
            }

            return await this.appointmentActivityRepository.AddAsync(appointmentActivityModel);
        }

        public async Task DeleteActivitiesOfAppointmentAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"Appointment with this id does not exist: {id}");
            }

            await this.appointmentActivityRepository.DeleteActivitiesOfAppointmentAsync(id);
        }

        public async Task DeleteAppointmentsOfActivityAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"Activity with this id does not exist: {id}");
            }
            await this.appointmentActivityRepository.DeleteAppointmentsOfActivityAsync(id);
        }

        public async Task DeleteAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"Appointment-activity with this id does not exist: {id}");
            }

            await this.appointmentActivityRepository.DeleteAsync(id);
        }

        public async Task<AppointmentActivityModel> GetByIdIfExistsAsync(int id)
        {
            return await this.appointmentActivityRepository.GetByIdIfExistsAsync(id);
        }

        public async Task UpdateAsync(AppointmentActivityModel appointmentActivityModel)
        {
            if (appointmentActivityModel == default)
            {
                throw new ArgumentNullException(nameof(appointmentActivityModel));
            }

            if (await this.GetByIdIfExistsAsync(appointmentActivityModel.Id) == default)
            {
                throw new ArgumentException($"Appointment-activity with this id does not exist: {appointmentActivityModel.Id}");
            }

            await this.appointmentActivityRepository.UpdateAsync(appointmentActivityModel);
        }
    }
}

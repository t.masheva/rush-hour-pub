﻿
using System.Collections.Generic;

namespace RushHour.Domain.Interfaces
{
    public interface ISecurityService
    {
        string GenerateJwtToken(string email, IEnumerable<string> roles);
    }
}

﻿using RushHour.DomainModels;
using System.Threading.Tasks;

namespace RushHour.Domain.Interfaces
{
    public interface IUserRoleService
    {
        Task DeleteAsync(int id);

        Task UpdateAsync(UserRoleModel userRoleModel);

        Task<UserRoleModel> AddAsync(UserRoleModel userRoleModel);

        Task<UserRoleModel> GetByIdIfExistsAsync(int id);
    }
}

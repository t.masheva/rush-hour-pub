﻿using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Interfaces
{
    public interface IUserService
    {
        Task DeleteAsync(int id);

        Task UpdateAsync(UserModel userModel);

        Task<UserModel> AddAsync(UserModel userModel);

        Task<UserModel> GetByIdIfExistsAsync(int id);

        Task<bool> CanUserLoginAsync(string email, string password);

        Task<UserModel> GetUserByEmailIfExistsAsync(string email);

        Task<IEnumerable<string>> GetRolesOfUserAsync(string email);
    }
}

﻿using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Interfaces
{
    public interface IActivityService
    {
        Task DeleteAsync(int id);

        Task UpdateAsync(ActivityModel activityModel);

        Task<ActivityModel> AddAsync(ActivityModel activityModel);

        Task<ActivityModel> GetByIdIfExistsAsync(int id);

        Task<IEnumerable<ActivityModel>> GetAllAsync();

        Task<ActivityModel> GetByNameIfExistsAsync(string name);

        Task<bool> AllNamesExistAsync(ICollection<string> activityNames);
    }
}

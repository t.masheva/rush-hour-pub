﻿using RushHour.DomainModels;
using System.Threading.Tasks;

namespace RushHour.Domain.Interfaces
{
    public interface IRoleService
    {
        Task DeleteAsync(int id);

        Task UpdateAsync(RoleModel roleModel);

        Task<RoleModel> AddAsync(RoleModel roleModel);

        Task<RoleModel> GetByIdIfExistsAsync(int id);

        public Task<int> GetIdAsync(RoleModel roleModel);

        public Task<bool> RoleByNameExistsAsync(string name);
    }
}

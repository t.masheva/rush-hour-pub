﻿using RushHour.DomainModels;
using System.Threading.Tasks;

namespace RushHour.Domain.Interfaces
{
    public interface IAppointmentActivityService
    {
        Task DeleteAsync(int id);

        Task UpdateAsync(AppointmentActivityModel appointmentActivityModel);

        Task<AppointmentActivityModel> AddAsync(AppointmentActivityModel appointmentActivityModel);

        Task<AppointmentActivityModel> GetByIdIfExistsAsync(int id);
        Task DeleteActivitiesOfAppointmentAsync(int id);
        Task DeleteAppointmentsOfActivityAsync(int id);
    }
}

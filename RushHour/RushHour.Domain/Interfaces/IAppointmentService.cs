﻿using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Interfaces
{
    public interface IAppointmentService
    {
        Task DeleteAsync(int id);

        Task UpdateAsync(AppointmentModel appointmentModel);

        Task<AppointmentModel> AddAsync(AppointmentModel appointmentModel);

        Task<AppointmentModel> GetByIdIfExistsAsync(int id);

        Task<IEnumerable<AppointmentModel>> GetAllAsync();

        Task<IEnumerable<AppointmentModel>> GetAppointmentsOfUserAsync(int userId);
    }
}

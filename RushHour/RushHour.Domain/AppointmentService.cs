﻿using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IAppointmentRepository appointmentRepository;
        private readonly IUserService userService;

        public AppointmentService(IAppointmentRepository appointmentRepository, IUserService userService)
        {
            this.appointmentRepository = appointmentRepository;
            this.userService = userService;
        }

        public async Task<AppointmentModel> AddAsync(AppointmentModel appointmentModel)
        {
            if (appointmentModel == default)
            {
                throw new ArgumentNullException(nameof(appointmentModel));
            }
            return await this.appointmentRepository.AddAsync(appointmentModel);
        }

        public async Task DeleteAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"Appointment with this id does not exist: {id}");
            }

            await this.appointmentRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<AppointmentModel>> GetAllAsync()
        {
            return await this.appointmentRepository.GetAllAsync();
        }

        public async Task<IEnumerable<AppointmentModel>> GetAppointmentsOfUserAsync(int userId)
        {
            if (await this.userService.GetByIdIfExistsAsync(userId) == default)
            {
                throw new ArgumentException("User with this id does not exist.");
            }

            return await this.appointmentRepository.GetAppointmentsOfUserAsync(userId);
        }

        public async Task<AppointmentModel> GetByIdIfExistsAsync(int id)
        {
            return await this.appointmentRepository.GetByIdIfExistsAsync(id);
        }

        public async Task UpdateAsync(AppointmentModel appointmentModel)
        {
            if (appointmentModel == default)
            {
                throw new ArgumentNullException(nameof(appointmentModel));
            }

            await this.appointmentRepository.UpdateAsync(appointmentModel);
        }
    }
}

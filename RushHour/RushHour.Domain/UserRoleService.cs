﻿using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Repositories.Interfaces;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IUserRoleRepository userRolesRepository;
        public UserRoleService(IUserRoleRepository userRolesRepository)
        {
            this.userRolesRepository = userRolesRepository;
        }

        public async Task<UserRoleModel> AddAsync(UserRoleModel userRoleModel)
        {
            if (userRoleModel == default)
            {
                throw new ArgumentNullException(nameof(userRoleModel));
            }

            return await this.userRolesRepository.AddAsync(userRoleModel);
        }

        public async Task DeleteAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"User-role with this id does not exist: {id}");
            }

            await this.userRolesRepository.DeleteAsync(id);
        }

        public async Task<UserRoleModel> GetByIdIfExistsAsync(int id)
        {
            return await this.userRolesRepository.GetByIdIfExistsAsync(id);
        }

        public async Task UpdateAsync(UserRoleModel userRoleModel)
        {
            if (userRoleModel == default)
            {
                throw new ArgumentNullException(nameof(userRoleModel));
            }

            if (await this.GetByIdIfExistsAsync(userRoleModel.Id) == default)
            {
                throw new ArgumentException($"User-role with this id does not exist: {userRoleModel.Id}");
            }

            await this.userRolesRepository.UpdateAsync(userRoleModel);
        }
    }
}

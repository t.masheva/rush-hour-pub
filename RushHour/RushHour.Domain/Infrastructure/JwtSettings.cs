﻿namespace RushHour.Domain.Infrastructure
{
    public class JwtSettings : IJwtSettings
    {
        public string Issuer { get; set; }
        public string Key { get; set; }
        public string Audience { get; set; }
        public int ExpirationInMinutes { get; set; }
    }

}

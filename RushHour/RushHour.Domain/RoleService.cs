﻿using RushHour.Domain.Interfaces;
using RushHour.DomainModels;
using RushHour.Repositories.Interfaces;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            this.roleRepository = roleRepository;
        }

        public async Task<RoleModel> AddAsync(RoleModel roleModel)
        {
            if (roleModel == default)
            {
                throw new ArgumentNullException(nameof(roleModel));
            }

            return await this.roleRepository.AddAsync(roleModel);
        }

        public async Task<int> GetIdAsync(RoleModel roleModel)
        {
            if (roleModel == default)
            {
                throw new ArgumentNullException(nameof(roleModel));
            }

            return await this.roleRepository.GetIdAsync(roleModel);
        }

        public async Task<bool> RoleByNameExistsAsync(string name)
        {
            return await this.roleRepository.RoleByNameExistsAsync(name);
        }

        public async Task DeleteAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"Role with this id does not exist: {id}");
            }

            await this.roleRepository.DeleteAsync(id);
        }

        public async Task<RoleModel> GetByIdIfExistsAsync(int id)
        {
            return await this.roleRepository.GetByIdIfExistsAsync(id);
        }

        public async Task UpdateAsync(RoleModel roleModel)
        {
            if (roleModel == default)
            {
                throw new ArgumentNullException(nameof(roleModel));
            }

            if (await this.GetByIdIfExistsAsync(roleModel.Id) == default)
            {
                throw new ArgumentException($"Role with this id does not exist: {roleModel.Id}");
            }

            await this.roleRepository.UpdateAsync(roleModel);
        }
    }
}

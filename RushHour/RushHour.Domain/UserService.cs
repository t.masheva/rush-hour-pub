﻿using RushHour.Domain.Interfaces;
using RushHour.Helpers;
using RushHour.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using RushHour.Repositories.Interfaces;

namespace RushHour.Domain
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IUserRoleService userRoleService;
        private readonly IRoleService roleService;

        public UserService(IUserRepository userRepository, IUserRoleService userRoleService, IRoleService roleService)
        {
            this.userRepository = userRepository;
            this.userRoleService = userRoleService;
            this.roleService = roleService;
        }

        public async Task DeleteAsync(int id)
        {
            if (await this.GetByIdIfExistsAsync(id) == default)
            {
                throw new ArgumentException($"User with this id does not exist: {id}");
            }

            await this.userRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(UserModel userModel)
        {
            if (userModel == default)
            {
                throw new ArgumentNullException(nameof(userModel));
            }

            if (await this.GetByIdIfExistsAsync(userModel.Id) == default)
            {
                throw new ArgumentException($"User with this id does not exist: {userModel.Id}");
            }

            await this.userRepository.UpdateAsync(userModel);
        }

        public async Task<UserModel> AddAsync(UserModel userModel)
        {
            if (userModel == default)
            {
                throw new ArgumentNullException(nameof(userModel));
            }

            return await this.userRepository.AddAsync(userModel); 
        }

        public async Task<UserModel> GetByIdIfExistsAsync(int id)
        {
            return await this.userRepository.GetByIdIfExistsAsync(id);
        }

        public async Task<bool> CanUserLoginAsync(string email, string password)
        {
            var hashedPassword = await this.userRepository.GetUserPasswordByEmailAsync(email);
            return PasswordHasher.VerifyPassword(password, hashedPassword);
        }

        public async Task<UserModel> GetUserByEmailIfExistsAsync(string email)
        {
            return await this.userRepository.GetUserByEmailIfExistsAsync(email);
        }

        public async Task<IEnumerable<string>> GetRolesOfUserAsync(string email)
        {
            var roles =  await this.userRepository.GetRolesOfUserAsync(email);
            return roles.Select(r => r.Name);
        }
    }
}

﻿using AutoMapper;
using RushHour.Data;
using RushHour.DomainModels;
using RushHour.Entities;
using RushHour.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Repositories
{
    public class AppointmentActivityRepository : BaseRepository<AppointmentActivity, AppointmentActivityModel>, IAppointmentActivityRepository
    {
        public AppointmentActivityRepository(RushHourDbContext rushHourDbContext, IMapper mapper) : base(rushHourDbContext, mapper)
        {
        }

        public async Task DeleteActivitiesOfAppointmentAsync(int id)
        {
            this.rushHourDbContext.AppointmentActivities.RemoveRange(this.rushHourDbContext.AppointmentActivities.Where(aa => aa.AppointmentId == id));           

            await rushHourDbContext.SaveChangesAsync();
        }

        public async Task DeleteAppointmentsOfActivityAsync(int id)
        {
            this.rushHourDbContext.AppointmentActivities.RemoveRange(this.rushHourDbContext.AppointmentActivities.Where(aa => aa.ActivityId == id));

            await rushHourDbContext.SaveChangesAsync();
        }
    }
}

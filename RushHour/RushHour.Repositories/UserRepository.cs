﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data;
using RushHour.Entities;
using RushHour.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RushHour.Repositories.Interfaces;

namespace RushHour.Repositories
{
    public class UserRepository : BaseRepository<User, UserModel>, IUserRepository
    {
        public UserRepository(RushHourDbContext rushHourDbContext, IMapper mapper) : base(rushHourDbContext, mapper)
        {
        }

        public async Task<UserModel> GetUserByEmailIfExistsAsync(string email)
        {
            var user = await this.rushHourDbContext.Users.FirstOrDefaultAsync(u => u.Email == email);

            var userModel = this.mapper.Map<UserModel>(user);

            return userModel;
        }

        public async Task<string> GetUserPasswordByEmailAsync(string email)
        {
            var user = await this.GetUserByEmailIfExistsAsync(email);

            if (user == default)
            {
                throw new ArgumentException($"User with this email does not exits: {email}");
            }

            return user.Password;
        }

        public async Task<IEnumerable<RoleModel>> GetRolesOfUserAsync(string email)
        {
            if (await this.GetUserByEmailIfExistsAsync(email) == default)
            {
                throw new ArgumentException($"User with this email does not exits: {email}");
            }

            return mapper.Map<IEnumerable<RoleModel>>((await this.rushHourDbContext.Users
                .FirstAsync(u => u.Email == email))
                .UserRoles.Select(ur => ur.Role));
        }
    }
}

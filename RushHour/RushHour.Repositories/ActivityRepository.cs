using AutoMapper;
using RushHour.Data;
using RushHour.Entities;
using RushHour.DomainModels;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using RushHour.Repositories.Interfaces;

namespace RushHour.Repositories
{
    public class ActivityRepository: BaseRepository<Activity, ActivityModel>, IActivityRepository
    {
        public ActivityRepository(RushHourDbContext rushHourDbContext, IMapper mapper) : base(rushHourDbContext, mapper)
        {
        }

        public async Task<ActivityModel> GetByNameIfExistsAsync(string name)
        {
            var activity = await this.rushHourDbContext.Activities.FirstOrDefaultAsync(a => a.Name == name);

            return this.mapper.Map<ActivityModel>(activity);
        }

        public async Task<bool> AllNamesExistAsync(ICollection<string> activityNames)
        {
            foreach (var name in activityNames)
            {
                if (await rushHourDbContext.Activities.AllAsync(a => a.Name != name))
                {
                    return false;
                }
            }

            return true;
        }
    }
}                                                                                                                                                                                                                                                                                                                                            
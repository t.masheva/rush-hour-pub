﻿using AutoMapper;
using RushHour.Data;
using RushHour.Entities;
using RushHour.DomainModels;
using RushHour.Repositories.Interfaces;

namespace RushHour.Repositories
{
    public class UserRoleRepository : BaseRepository<UserRole, UserRoleModel>, IUserRoleRepository
    {
        public UserRoleRepository(RushHourDbContext rushHourDbContext, IMapper mapper) : base(rushHourDbContext, mapper)
        {

        }
    }
}

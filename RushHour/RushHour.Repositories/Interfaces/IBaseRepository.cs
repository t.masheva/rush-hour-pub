﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Repositories.Interfaces
{
    public interface IBaseRepository<TModel>
    {
        Task<IEnumerable<TModel>> GetAllAsync();

        Task<TModel> GetByIdIfExistsAsync(int id);

        Task<TModel> AddAsync(TModel model);

        Task UpdateAsync(TModel model);

        Task DeleteAsync(int id);
    }
}

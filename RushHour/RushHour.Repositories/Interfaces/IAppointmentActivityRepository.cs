﻿using RushHour.DomainModels;
using System.Threading.Tasks;

namespace RushHour.Repositories.Interfaces
{
    public interface IAppointmentActivityRepository : IBaseRepository<AppointmentActivityModel>
    {
        Task DeleteActivitiesOfAppointmentAsync(int id);

        Task DeleteAppointmentsOfActivityAsync(int id);
    }
}

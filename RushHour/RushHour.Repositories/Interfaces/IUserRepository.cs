﻿using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<UserModel>
    {
        Task<UserModel> GetUserByEmailIfExistsAsync(string email);

        Task<string> GetUserPasswordByEmailAsync(string email);

        Task<IEnumerable<RoleModel>> GetRolesOfUserAsync(string email);
    }
}

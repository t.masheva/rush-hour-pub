﻿using RushHour.DomainModels;

namespace RushHour.Repositories.Interfaces
{
    public interface IUserRoleRepository : IBaseRepository<UserRoleModel>
    {
    }
}

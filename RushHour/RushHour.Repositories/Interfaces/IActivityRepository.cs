﻿using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Repositories.Interfaces
{
    public interface IActivityRepository : IBaseRepository<ActivityModel>
    {
        Task<ActivityModel> GetByNameIfExistsAsync(string name);

        Task<bool> AllNamesExistAsync(ICollection<string> activityNames);
    }
}

﻿using RushHour.DomainModels;
using System.Threading.Tasks;

namespace RushHour.Repositories.Interfaces
{
    public interface IRoleRepository : IBaseRepository<RoleModel>
    {
        Task<int> GetIdAsync(RoleModel roleModel);

        Task<bool> RoleByNameExistsAsync(string name);
    }
}

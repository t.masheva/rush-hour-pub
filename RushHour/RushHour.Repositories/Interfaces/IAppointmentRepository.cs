﻿using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Repositories.Interfaces
{
    public interface IAppointmentRepository : IBaseRepository<AppointmentModel>
    {
        Task<IEnumerable<AppointmentModel>> GetAppointmentsOfUserAsync(int userId);
    }
}

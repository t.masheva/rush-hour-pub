﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data;
using RushHour.Entities;
using RushHour.DomainModels;
using System.Threading.Tasks;
using RushHour.Repositories.Interfaces;
using System;

namespace RushHour.Repositories
{
    public class RoleRepository : BaseRepository<Role, RoleModel>, IRoleRepository
    {
        public RoleRepository(RushHourDbContext rushHourDbContext, IMapper mapper) : base(rushHourDbContext, mapper)
        {
        }

        public async Task<int> GetIdAsync(RoleModel roleModel)
        {
            if (roleModel == default)
            {
                throw new ArgumentNullException(nameof(roleModel));
            }

            return (await this.rushHourDbContext.Roles.FirstOrDefaultAsync(r => r.Name == roleModel.Name)).Id;
        }

        public async Task<bool> RoleByNameExistsAsync(string name)
        {
            return await this.rushHourDbContext.Roles.AnyAsync(r => r.Name == name);
        }
    }
}

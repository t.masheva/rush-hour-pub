using AutoMapper;
using RushHour.Data;
using RushHour.Entities;
using RushHour.DomainModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RushHour.Repositories.Interfaces;

namespace RushHour.Repositories
{
    public class AppointmentRepository : BaseRepository<Appointment, AppointmentModel>, IAppointmentRepository
    {
        public AppointmentRepository(RushHourDbContext rushHourDbContext, IMapper mapper) : base(rushHourDbContext, mapper)
        {
        }

        public async Task<IEnumerable<AppointmentModel>> GetAppointmentsOfUserAsync(int userId)
        {
            var appointments = await this.rushHourDbContext.Appointments.Where(a => a.UserId == userId).ToListAsync();
            return mapper.Map<IEnumerable<AppointmentModel>>(appointments);
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
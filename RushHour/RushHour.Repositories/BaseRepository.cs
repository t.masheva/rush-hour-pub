﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data;
using RushHour.Entities;
using RushHour.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using RushHour.Repositories.Interfaces;
using System;

namespace RushHour.Repositories
{
    public abstract class BaseRepository<TEntity, TModel> : IBaseRepository<TModel>
        where TEntity : BaseEntity
        where TModel : BaseModel
    {
        protected readonly RushHourDbContext rushHourDbContext;
        protected readonly IMapper mapper;

        protected BaseRepository(RushHourDbContext rushHourDbContext, IMapper mapper)
        {
            this.rushHourDbContext = rushHourDbContext;
            this.mapper = mapper;
        }

        public async virtual Task<IEnumerable<TModel>> GetAllAsync()
        {
            return mapper.Map<IEnumerable<TModel>>(await rushHourDbContext.Set<TEntity>().ToListAsync());
        }

        public virtual async Task<TModel> GetByIdIfExistsAsync(int id)
        {
            var entity = await rushHourDbContext.FindAsync<TEntity>(id);
            var model = mapper.Map<TEntity, TModel>(entity);
            return model;
        }

        public async Task<TModel> AddAsync(TModel model)
        {
            if (model == default)
            {
                throw new ArgumentNullException(nameof(model));
            }

            var entity = mapper.Map<TModel, TEntity>(model);
            await rushHourDbContext.Set<TEntity>().AddAsync(entity);

            await rushHourDbContext.SaveChangesAsync();

            model = mapper.Map<TModel>(entity);
            return model;
        }
        public async Task UpdateAsync(TModel model)
        {
            if (model == default)
            {
                throw new ArgumentNullException(nameof(model));
            }

            var entity = await rushHourDbContext.Set<TEntity>().FindAsync(model.Id);

            var mappedEntity = mapper.Map(model, entity);
            rushHourDbContext.Set<TEntity>().Update(mappedEntity);

            await rushHourDbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await rushHourDbContext.Set<TEntity>().FindAsync(id);

            if (entity == default)
            {
                throw new ArgumentException($"Cannot delete entity with this id because it does not exist: {id}");
            }

            rushHourDbContext.Set<TEntity>().Remove(entity);

            await rushHourDbContext.SaveChangesAsync();
        }

    }
}
